def manage_teacher_free_times(name, free_times, param_pro):
    if free_times == "all":
        param_pro.set_free_time_to_all(name)
    else:
        for time in free_times.split('_'):
            if "(" in time:
                day, hours = time.split("(")
                hours = hours[:-1]
                for hour in hours.split("|"):
                    param_pro.add_free_time(name, int(day), int(hour))
            else:
                for hour in param_pro.hours:
                    param_pro.add_free_time(name, int(time), hour)


def manage_hour_spec(name, hour_specific, param_pro):
    for item in hour_specific.split("$"):
        field, hours = item.split(":")
        param_pro.add_hour_spec_and_field(name, field, int(hours))


def fill_from_file(file_name, param_pro=None):
    current_scope = None
    with open(file_name) as f:
        for line in f:
            if line[-1] == "\n":
                line = line[:- 1]
            if line == "end":
                break
            if line == "":
                continue
            if line == "day":
                current_scope = "day"
            elif line == "hour":
                current_scope = "hour"
            elif line == "course":
                current_scope = "course"
            elif line == "class":
                current_scope = "class"
            elif line == "curriculum":
                current_scope = "curriculum"
            elif line == "teacher":
                current_scope = "teacher"
            if line == "}" and current_scope == "hour":
                param_pro.generate_times()
            if line == "{" or line == "}" or line == "day" or line == "hour" or line == "course" or line == "class" or line == "curriculum" or line == "teacher":
                continue
            if current_scope == "day":
                for day in line.split(','):
                    param_pro.add_day(int(day))
            if current_scope == "hour":
                for hour in line.split(','):
                    param_pro.add_hour(int(hour))
            if current_scope == "course":
                for course_name in line.split(','):
                    param_pro.add_course(course_name)
            if current_scope == "class":
                grade, count = line.split(":")
                for i in range(int(count)):
                    param_pro.add_class(grade + "_" + str(i + 1))
            if current_scope == "curriculum":
                grade, course_amounts = line.split("--")
                course_amounts = course_amounts.split(",")
                for course_amount in course_amounts:
                    course, amount = course_amount.split(":")
                    param_pro.add_class_curriculum(grade, course, int(amount))
            if current_scope == "teacher":
                name, hour, free_times, max_days, hour_spec = line.split(",")
                name_params = name.split("(")
                name = name_params[0]
                param_pro.add_teacher(name)
                if len(name_params) > 1 and name_params[1] == "assistant)":
                    param_pro.set_assistant(name)
                param_pro.set_teacher_hour(name, int(hour))
                param_pro.set_teacher_max_days(name, int(max_days))
                manage_teacher_free_times(name, free_times, param_pro)
                manage_hour_spec(name, hour_spec, param_pro)