import csv


class NoSuchElementException(Exception):
    pass


class CsvImporter:
    def __init__(self, param_provider):
        self.days = param_provider.days
        self.hours = param_provider.hours
        self.courses_itn = param_provider.courses_itn
        self.courses_nti = param_provider.courses_nti
        self.teachers_itn = param_provider.teachers_itn
        self.teachers_nti = param_provider.teachers_nti
        self.classes_itn = param_provider.classes_itn
        self.classes_nti = param_provider.classes_nti
        self.times = param_provider.times

    def import_csv(self, file_name):
        timetable = {}
        with open(file_name) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            is_first = True
            for row in csv_reader:
                if is_first:
                    is_first = False
                    continue
                teacher_name = row[0]
                if teacher_name not in self.teachers_nti:
                    raise NoSuchElementException(teacher_name)

                for i in range(1, len(row)):
                    if row[i] == " ":
                        timetable.setdefault(self.teachers_nti[teacher_name], {})[self.times[i - 1]] = (-1, -1)
                    else:
                        course_name, class_name = tuple(row[i].split("/"))
                        if course_name not in self.courses_nti:
                            raise NoSuchElementException(course_name + " not recognized in line: " + teacher_name)
                        if class_name not in self.classes_nti:
                            raise NoSuchElementException(class_name + " not recognized in line: " + teacher_name)
                        timetable.setdefault(self.teachers_nti[teacher_name], {})[self.times[i - 1]] = (
                            self.courses_nti[course_name], self.classes_nti[class_name])
        return timetable
