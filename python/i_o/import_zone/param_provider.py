import itertools


class DuplicateElement(Exception):
    pass


class InvalidTime(Exception):
    pass


class NOSuchElement(Exception):
    pass


class SchoolHoursMismatch(Exception):
    pass


class TeacherTotalAndSpecJobHoursMismatch(Exception):
    pass


class FieldRequiredHoursNotMeet(Exception):
    pass


class TeacherHourSpecCourseListSuperImpose(Exception):
    pass


class GradeCurriculumSumError(Exception):
    pass


def get_product(*x):
    return list(itertools.product(*x))


class ParamProvider:
    """ itn stands for id to name
        nti stands for name to id
        itg stands for id to grade
        gti stands for grade to id
    """

    def __init__(self):
        self.days, self.hours, self.times, self.courses_itn = [], [], [], {}
        self.courses_nti, self.teachers_itn, self.teachers_nti, self.classes_itn = {}, {}, {}, {}
        self.classes_nti, self.classes_itg, self.class_curriculum = {}, {}, {}
        self.teachers_free_times, self.teacher_fields, self.teacher_hours_count = {}, {}, {}
        self.teacher_max_days, self.teacher_hour_spec = {}, {}
        self.teacher_job_hours_calculated = {}
        self.fields = []
        self.is_teacher_assistant = {}
        self.classes_gti = {}

    def add_day(self, day):
        self.days.append(day)

    def add_hour(self, hour):
        self.hours.append(hour)

    def generate_times(self):
        self.times = get_product(self.days, self.hours)

    def add_course(self, name):
        if name in self.courses_nti:
            raise DuplicateElement(name)

        course_id = len(self.courses_itn)
        self.courses_itn[course_id] = name
        self.courses_nti[name] = course_id
        field = name.split('_')[0]
        if field not in self.fields:
            self.fields.append(field)

    def add_teacher(self, name):
        if name in self.teachers_nti:
            raise DuplicateElement(name)

        teacher_id = len(self.teachers_itn)
        self.teachers_itn[teacher_id] = name
        self.teachers_nti[name] = teacher_id
        self.is_teacher_assistant[teacher_id] = False

    def add_class(self, name):
        if name in self.classes_nti:
            raise DuplicateElement(name)

        class_id = len(self.classes_itn)
        self.classes_itn[class_id] = name
        self.classes_nti[name] = class_id
        grade = name.split("_")[0]
        self.classes_itg[class_id] = grade
        self.classes_gti.setdefault(grade, []).append(class_id)

    def set_assistant(self, name):
        if name not in self.teachers_nti:
            raise NOSuchElement(name)

        self.is_teacher_assistant[self.teachers_nti[name]] = True

    def add_free_time(self, name, day, hour):
        if day not in self.days or hour not in self.hours:
            raise InvalidTime("teacher: " + name + " day: " + day + " hour: " + hour)
        if name not in self.teachers_nti:
            raise NOSuchElement(name)

        teacher_id = self.teachers_nti[name]
        self.teachers_free_times.setdefault(teacher_id, []).append((day, hour))

    def set_free_time_to_all(self, name):
        if name not in self.teachers_nti:
            raise NOSuchElement(name)
        self.teachers_free_times[self.teachers_nti[name]] = self.times

    def add_field(self, teacher_id, course_id):
        if teacher_id not in [item[0] for item in self.teachers_itn.items()] or course_id not in [item[0] for item in
                                                                                                  self.courses_itn.items()]:
            raise NOSuchElement
        if teacher_id in [item[0] for item in self.teacher_fields.items()]:
            self.teacher_fields[teacher_id] = set(list(self.teacher_fields[teacher_id]) + [course_id])
        else:
            self.teacher_fields[teacher_id] = [course_id]

    def set_teacher_hour(self, name, amount):
        if name not in self.teachers_nti:
            raise NOSuchElement(name)

        self.teacher_hours_count[self.teachers_nti[name]] = amount

    def add_class_curriculum(self, grade, course_name, units):
        if grade not in self.classes_gti:
            raise NOSuchElement(grade)
        if course_name not in self.courses_nti:
            raise NOSuchElement(course_name)

        self.class_curriculum.setdefault(grade, {})[self.courses_nti[course_name]] = units

    def set_teacher_max_days(self, name, amount):
        if name not in self.teachers_nti:
            raise NOSuchElement(name)
        self.teacher_max_days[self.teachers_nti[name]] = amount

    def add_hour_spec_and_field(self, name, field, amount):
        courses = [self.courses_nti[course_name] for course_name in self.courses_nti if field in course_name]
        if name not in self.teachers_nti:
            raise NOSuchElement(name)
        if len(courses) == 0:
            raise NOSuchElement("no field with name:" + field)

        teacher_id = self.teachers_nti[name]
        self.teacher_hour_spec.setdefault(teacher_id, {})[tuple(courses)] = amount
        self.teacher_fields.setdefault(teacher_id, []).extend(courses)

    def get_params(self):
        self.normalize_teacher_hour_spec()
        self.normalize_teacher_fields()
        self.check_validity()
        x = self.days, self.hours, self.times, self.courses_itn, \
            self.teachers_itn, self.classes_itn, self.teacher_fields, \
            self.teachers_free_times, self.teacher_hours_count, self.classes_itg, self.class_curriculum, \
            self.teacher_max_days, self.teacher_hour_spec, self.fields, self.is_teacher_assistant
        return x

    def check_validity(self):
        self.check_local_teacher_hours()
        self.check_fields_hours()
        self.check_grades_curriculum()
        self.check_school_total_hours()

    def check_local_teacher_hours(self):
        for teacher_id in self.teacher_hour_spec:
            course_lists = self.teacher_hour_spec[teacher_id]
            calculated_hours = 0
            courses_covered = []
            for course_list in course_lists:
                for course in course_list:
                    if course in courses_covered:
                        raise TeacherHourSpecCourseListSuperImpose(
                            teacher_id + "'s course_lists super impose with each other!")
                    courses_covered.append(course)
                calculated_hours += self.teacher_hour_spec[teacher_id][course_list]
            if calculated_hours != self.teacher_hours_count[teacher_id]:
                raise TeacherTotalAndSpecJobHoursMismatch(self.teachers_itn[teacher_id] + " should be: " + str(
                    self.teacher_hours_count[teacher_id]) + "h but is: " + str(calculated_hours) + "h")

    def check_fields_hours(self):
        for field in self.fields:
            offered_hours = sum(
                self.teacher_hour_spec[teacher_id][course_list] for teacher_id in self.teacher_hour_spec for course_list
                in self.teacher_hour_spec[teacher_id] if field in self.courses_itn[course_list[0]])
            expected_hours = sum(
                self.class_curriculum[self.classes_itg[class_id]][course_id] for class_id in self.classes_itn for
                course_id in self.class_curriculum[self.classes_itg[class_id]] if field in self.courses_itn[course_id])
            if expected_hours != offered_hours:
                raise FieldRequiredHoursNotMeet(
                    field + " should be: " + str(expected_hours) + "h but is:" + str(offered_hours) + "h")

    def check_grades_curriculum(self):
        expected_hours = len(self.hours) * len(self.days)
        for grade in self.class_curriculum:
            specialized_hours = sum(self.class_curriculum[grade][course] for course in self.class_curriculum[grade])
            if expected_hours != specialized_hours:
                raise GradeCurriculumSumError(grade)

    def check_school_total_hours(self):
        if sum(list(self.teacher_hours_count.values())) != len(self.days) * len(self.hours) * len(
                self.classes_itn):
            raise SchoolHoursMismatch("should be: " + str(len(self.days) * len(self.hours) * len(self.classes_itn))
                                      + "h but is: " + str(
                sum([hour for _, hour in self.teacher_hours_count.items()])) + "h")

    def normalize_teacher_hour_spec(self):
        for teacher_id, course_package_amounts in self.teacher_hour_spec.items():
            covered_courses = []
            for course_list in sorted(course_package_amounts, key=lambda x: len(x)):
                previously_met_courses = [course for course in course_list if course in covered_courses]
                covered_courses.extend(list(course_list))
                if len(previously_met_courses) != 0:
                    self.teacher_hour_spec[teacher_id][
                        tuple([course for course in course_list if course not in previously_met_courses])] = \
                        self.teacher_hour_spec[teacher_id].pop(course_list) - sum(self.teacher_hour_spec[teacher_id][
                                                                                      tuple([course])] for course in
                                                                                  previously_met_courses)

    def normalize_teacher_fields(self):
        for teacher_id in self.teacher_fields:
            self.teacher_fields[teacher_id] = list(set(self.teacher_fields[teacher_id]))

    def get_the_most_constraining_days_log(self):
        constraint_log = {}
        for time in get_product(self.days, self.hours):
            for teacher in self.teachers_itn:
                if time in self.teachers_free_times[teacher] and len(self.teachers_free_times[teacher]) != len(
                        self.days) * len(self.hours):
                    constraint_log[time] = constraint_log.setdefault(time, 0) + 1
        return sorted(constraint_log.items(), key=lambda x: x[1], reverse=True)
