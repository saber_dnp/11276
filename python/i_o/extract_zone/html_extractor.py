from selenium import webdriver


class HtmlExtractor:
    def __init__(self, param_provider):
        self.solution_version = 0
        self.days = param_provider.days
        self.hours = param_provider.hours
        self.times = param_provider.times
        self.courses_itn = param_provider.courses_itn
        self.teachers_itn = param_provider.teachers_itn
        self.classes_itn = param_provider.classes_itn
        self.week_days = {0: "Saturday", 1: "Sunday", 2: "Monday", 3: "Tuesday", 4: "Wednesday", 5: "Thursday",
                          6: "Friday"}
        self.columns = []
        for day in self.days:
            for hour in self.hours:
                self.columns.append(self.week_days[day] + ", " + str(hour + 1))

    def extract_to_html(self, schedule):
        browser = webdriver.Firefox()
        browser.get('file:///home/saber/PhpstormProjects/untitled/schedule.html')

        # teacherName.concat(" Schedule with ".concat("<span class=\"small\">").concat(statistics).concat("</span>"))
        table_id = "teachers-table"
        table_caption = "Teachers Schedules"
        table_data = self.encode_teachers_table(schedule)
        browser.execute_script("createTable(arguments[0], arguments[1], arguments[2], arguments[3])", table_data,
                               table_caption, table_id, "false")

        table_id = "classes-table"
        table_caption = "Classes Schedules"
        table_data = self.encode_classes_table(schedule)
        browser.execute_script("createTable(arguments[0], arguments[1], arguments[2], arguments[3])", table_data,
                               table_caption, table_id, "false")

        for teacher_id in list(map(lambda x: x[0], sorted(self.teachers_itn.items(), key=lambda x: x[1]))):
            table_id = self.teachers_itn[teacher_id]
            table_caption = table_id + " Schedule"
            table_data = self.encode_specific_teacher_table(schedule, teacher_id)
            browser.execute_script("createTable(arguments[0], arguments[1], arguments[2], arguments[3])", table_data,
                                   table_caption, table_id, "true")

        timetable = schedule.timetable
        class_schedule = {}
        for class_id in self.classes_itn:
            for time in self.times:
                class_schedule.setdefault(class_id, {})[time] = (-1, -1)

        for teacher_id in timetable:
            for time in timetable[teacher_id]:
                course, class_id = timetable[teacher_id][time]
                if class_id != -1:
                    class_schedule[class_id][time] = (course, teacher_id)

        for class_id in self.classes_itn:
            table_id = self.classes_itn[class_id]
            table_caption = table_id + " Schedule"
            table_data = self.encode_specific_class_table(class_schedule, class_id)
            browser.execute_script("createTable(arguments[0], arguments[1], arguments[2], arguments[3])", table_data,
                                   table_caption, table_id, "false")

        browser.execute_script("addFooter()")
        f = open("/home/saber/PhpstormProjects/untitled/new_html.html", "w")
        f.write(browser.page_source)
        f.close()

    def encode_teachers_table(self, schedule):
        timetable = schedule.timetable
        string = "Teacher&"
        for time in self.columns:
            string += time + "&"
        string = string[: len(string) - 1] + "^"
        for teacher_id in list(map(lambda x: x[0], sorted(self.teachers_itn.items(), key=lambda x: x[1]))):
            string += self.teachers_itn[teacher_id] + "&"
            for time in sorted(self.times):
                course, class_id = timetable[teacher_id][time]
                if course != -1:
                    string += self.courses_itn[course] + "/" + self.classes_itn[class_id]
                else:
                    string += " "
                string += "&"
            string = string[: len(string) - 1] + "^"
        string = string[: len(string) - 1]
        return string

    def encode_classes_table(self, schedule):
        timetable = schedule.timetable
        class_schedule = {}
        for class_id in self.classes_itn:
            for time in self.times:
                class_schedule.setdefault(class_id, {})[time] = (-1, -1)

        for teacher_id in timetable:
            for time in timetable[teacher_id]:
                course, class_id = timetable[teacher_id][time]
                if class_id != -1:
                    class_schedule[class_id][time] = (course, teacher_id)
        string = "Class&"
        for time in self.columns:
            string += time + "&"
        string = string[: len(string) - 1] + "^"
        for class_id in self.classes_itn:
            string += self.classes_itn[class_id] + "&"
            for time in sorted(self.times):
                course, teacher_id = class_schedule[class_id][time]
                if course != -1:
                    string += self.courses_itn[course] + "/" + self.teachers_itn[teacher_id]
                else:
                    string += " "
                string += "&"
            string = string[: len(string) - 1] + "^"
        string = string[: len(string) - 1]
        return string

    def encode_specific_teacher_table(self, schedule, teacher_id):
        string = "Day&"
        for hour in self.hours:
            string += str(hour + 1) + "&"
        string = string[: len(string) - 1] + "^"
        for day in self.days:
            string += self.week_days[day] + "&"
            for hour in self.hours:
                course, class_id = schedule.timetable[teacher_id][(day, hour)]
                if course != -1:
                    string += self.courses_itn[course] + "/" + self.classes_itn[class_id]
                else:
                    string += " "
                string += "&"
            string = string[: len(string) - 1] + "^"
        string = string[: len(string) - 1]
        return string

    def encode_specific_class_table(self, class_schedule, target_class_id):
        string = "Day&"
        for hour in self.hours:
            string += str(hour + 1) + "&"
        string = string[: len(string) - 1] + "^"
        for day in self.days:
            string += self.week_days[day] + "&"
            for hour in self.hours:
                course, teacher_id = class_schedule[target_class_id][(day, hour)]
                if course != -1:
                    string += self.courses_itn[course] + "/" + self.teachers_itn[teacher_id]
                else:
                    string += " "
                string += "&"
            string = string[: len(string) - 1] + "^"
        string = string[: len(string) - 1]
        return string
