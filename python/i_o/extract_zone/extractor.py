from i_o.extract_zone.csv_extractor import CsvExtractor
from i_o.extract_zone.html_extractor import HtmlExtractor
from i_o.extract_zone.txt_extractor import TxtExtractor


class Extractor:
    def __init__(self, param_provider):
        self.output_directory = "../../i_o/extract_zone/csv_files/"
        self.txt_extractor = TxtExtractor(self.output_directory + "candidate_solutions/", param_provider)
        self.html_extractor = HtmlExtractor(param_provider)
        self.csv_extractor = CsvExtractor(self.output_directory + "final_output/", param_provider)
        self.extraction_history = []

    def extract_txt(self, schedule):
        self.txt_extractor.extract_to_txt(schedule, len(self.extraction_history))
        self.extraction_history.append(schedule)

    def extract_final_edition(self):
        user_choice = 0
        while True:
            try:
                user_choice = int(input("enter id of the schedule you are most interested in:"))
                if user_choice >= len(self.extraction_history):
                    print("Sorry, but there isn't such schedule id, try another one...")
                    continue
                confirm_statement = input("sure?y/n")
                if confirm_statement == "y":
                    break
                elif confirm_statement != "n":
                    print("WTF: it's so simple: y or n!")
            except:
                print("for goodness sake enter number :(")
                continue
        self.csv_extractor.extract_to_csv(self.extraction_history[user_choice])
        self.html_extractor.extract_to_html(self.extraction_history[user_choice])
