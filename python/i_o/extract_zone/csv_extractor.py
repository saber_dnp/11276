import csv


class CsvExtractor:
    def __init__(self, output_directory, param_provider):
        self.output_directory = output_directory
        self.days = param_provider.days
        self.hours = param_provider.hours
        self.courses_itn = param_provider.courses_itn
        self.teachers_itn = param_provider.teachers_itn
        self.classes_itn = param_provider.classes_itn
        self.week_days = {0: "Saturday", 1: "Sunday", 2: "Monday", 3: "Tuesday", 4: "Wednesday", 5: "Thursday",
                          6: "Friday"}
        self.columns = ["teacher"]
        for day in self.days:
            for hour in self.hours:
                self.columns.append(self.week_days[day] + ", " + str(hour + 1))

    def extract_to_csv(self, schedule):
        dict_data = []
        for teacher_id in list(map(lambda x: x[0], sorted(self.teachers_itn.items(), key=lambda x: x[1]))):
            dic = {}
            column_num = 0
            dic[self.columns[column_num]] = self.teachers_itn[teacher_id]
            column_num += 1
            for day in self.days:
                for hour in self.hours:
                    course, class_id = schedule.timetable[teacher_id][(day, hour)]
                    if course != -1:
                        dic[self.columns[column_num]] = str(self.courses_itn[course]) + "/" + str(self.classes_itn[class_id])
                    else:
                        dic[self.columns[column_num]] = " "
                    column_num += 1
            dict_data.append(dic)
        try:
            with open(self.output_directory + "schedule.csv", 'w') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=self.columns)
                writer.writeheader()
                for data in dict_data:
                    writer.writerow(data)
        except IOError:
            print("I/O error")
