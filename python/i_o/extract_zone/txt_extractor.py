import csv

from i_o.utils.table import Table


class TxtExtractor:
    def __init__(self, output_directory, param_provider):
        self.output_directory = output_directory
        self.days = param_provider.days
        self.hours = param_provider.hours
        self.courses_itn = param_provider.courses_itn
        self.teachers_itn = param_provider.teachers_itn
        self.classes_itn = param_provider.classes_itn
        self.week_days = {0: "Saturday", 1: "Sunday", 2: "Monday", 3: "Tuesday", 4: "Wednesday", 5: "Thursday",
                          6: "Friday"}
        self.columns = ["teacher"]
        for day in self.days:
            for hour in self.hours:
                self.columns.append(self.week_days[day] + ", " + str(hour + 1))

    def extract_to_txt(self, schedule, sche_id):
        f = open(self.output_directory + "schedule" + str(sche_id), "w")
        table = Table(f)
        self.print_schedule_whole(schedule, table)
        f.close()

    def print_schedule_whole(self, schedule, table):
        table.put_into_next_line("schedule specifications: (score, gaps, single_bells, excess days, super impose, out of free times, excess_lessons_count) = " + str(schedule.score) + ", " + str(schedule.gaps_count) + ", " + str(schedule.single_bells_count) + ", " + str(schedule.excess_days_count) + ", " + str(schedule.class_super_impose_count) + ", " + str(schedule.out_of_free_times_count) + ", " + str(schedule.excess_lessons_count))
        table.put_into_next_line("teacher")
        for day in self.days:
            for hour in self.hours:
                table.put_into_next_column(self.week_days[day] + ", " + str(hour + 1))
        table.put_into_next_column("gaps")
        table.put_into_next_column("excess days")
        table.put_into_next_column("max_days")
        table.put_into_next_column("excess_lessons")
        for teacher_id in list(map(lambda x: x[0], sorted(self.teachers_itn.items(), key=lambda x: x[1]))):
            table.put_into_next_line(self.teachers_itn[teacher_id])
            for day in self.days:
                for hour in self.hours:
                    course_id, class_id = schedule.timetable[teacher_id][(day, hour)]
                    if course_id == -1:
                        table.put_into_next_column(" ")
                    else:
                        table.put_into_next_column(self.courses_itn[course_id] + "/" + self.classes_itn[class_id])
            table.put_into_next_column(str(schedule.get_gaps_count(teacher_id)))
            table.put_into_next_column(str(schedule.get_excess_days_count(teacher_id)))
            table.put_into_next_column(str(schedule.analyser.teacher_max_days[teacher_id]))
            table.put_into_next_column(str(schedule.get_excess_lessons_count(teacher_id)))
