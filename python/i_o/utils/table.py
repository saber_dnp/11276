class Table:

    def __init__(self, file):
        self.f = file
        self.column_width = 30
        self.column_pos = 0

    def put_into_next_column(self, string):
        self.f.write(string)
        self.column_pos += len(string)
        self.adjust()

    def put_into_next_line(self, string):
        self.f.write("\n" + string)
        self.column_pos = len(string)
        self.adjust()

    def adjust(self):
        while self.column_pos % self.column_width != 0:
            self.f.write(" ")
            self.column_pos += 1

    def partial_separate(self):
        self.put_into_next_line("___________")

    def separate(self):
        for _ in range(2):
            self.put_into_next_line(
                "________________________________________________________________________________________________________")
