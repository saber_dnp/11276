import copy
import time

from approaches.tabu_search.schedule import Schedule


def get_extreme_big_small_values(day_busy_times, cur_hour):
    last_smaller_hour = -1
    first_bigger_hour = 100
    for _, hour in day_busy_times:
        if hour < cur_hour and hour > last_smaller_hour:
            last_smaller_hour = hour
        elif hour > cur_hour and hour < first_bigger_hour:
            first_bigger_hour = hour
    if first_bigger_hour == 100:
        first_bigger_hour = -1
    return last_smaller_hour, first_bigger_hour


def get_relative_gap_amount_by_smaller_bigger_value(last_smaller_hour, first_bigger_hour, cur_hour, insert):
    relative = 0
    if last_smaller_hour != -1:
        if first_bigger_hour != -1:
            relative -= 1
        else:
            relative += cur_hour - 1 - last_smaller_hour
    else:
        if first_bigger_hour != -1:
            relative += first_bigger_hour - 1 - cur_hour
    if insert:
        return relative
    else:
        return -relative


def get_is_in_free_times_dict(teacher_free_times, times):
    is_in_free_times = {}
    for teacher in teacher_free_times:
        for time in times:
            is_in_free_times.setdefault(teacher, {})[time] = time in teacher_free_times[teacher]
    return is_in_free_times


class Analyser:
    def __init__(self, infeasibility_imp, gap_imp, single_bell_imp, max_days_imp, excess_lessons_imp, max_lessons_each_day, param_provider):
        self.days, self.hours, self.times, self.courses_itn, self.teachers_itn, self.classes_itn, self.teacher_fields, self.teacher_free_times, self.teacher_job_hours, self.class_grades, self.class_curriculum, self.teacher_max_days, self.hour_spec, self.fields, self.is_assistant = param_provider.get_params()
        self.infeasibility_imp = infeasibility_imp
        self.gap_imp = gap_imp
        self.single_bell_imp = single_bell_imp
        self.max_days_imp = max_days_imp
        self.excess_lessons_imp = excess_lessons_imp
        self.odd_bells_school = len(self.hours) % 2 != 0
        self.is_in_free_times = get_is_in_free_times_dict(self.teacher_free_times, self.times)
        self.max_lessons_each_day = max_lessons_each_day
        self.param_provider = param_provider
        self.teacher_max_days_penalize_weights = self.get_max_days_penalize_weights()
        print(self.teacher_max_days_penalize_weights)

    def get_transformed_schedule(self, schedule, moves, analyse_outputs):
        timetable = schedule.timetable
        time_classes = schedule.time_classes
        teacher_busy_times_by_day = schedule.teacher_busy_times_by_day
        for teacher, pat_time, mat_time in moves:
            pat_class = timetable[teacher][pat_time][1]
            mat_class = timetable[teacher][mat_time][1]
            self.update_timetable(teacher, pat_time, mat_time, timetable)
            self.update_time_classes(time_classes, pat_time, mat_time, pat_class, mat_class)
            self.update_teacher_busy_times_by_day(teacher, pat_time, mat_time, pat_class, mat_class, teacher_busy_times_by_day)
        gaps_count, single_bells_count, excess_days_count, class_super_impose_count, out_of_free_times_count, score, excess_lessons_count = analyse_outputs
        new_schedule = Schedule(self, timetable, time_classes, teacher_busy_times_by_day, gaps_count, single_bells_count, excess_days_count, class_super_impose_count, out_of_free_times_count, score, excess_lessons_count)
        return new_schedule

    def update_timetable(self, teacher, pat_time, mat_time, timetable):
        temp = timetable[teacher][pat_time]
        timetable[teacher][pat_time] = timetable[teacher][mat_time]
        timetable[teacher][mat_time] = temp

    def update_teacher_busy_times_by_day(self, teacher, pat_time, mat_time, pat_class, mat_class, teacher_busy_times_by_day):
        cur_class, cur_time, other_time = pat_class, pat_time, mat_time
        for i in range(2):
            if cur_class == -1:
                teacher_busy_times_by_day[teacher].setdefault(cur_time[0], []).append(cur_time)
                if len(teacher_busy_times_by_day[teacher][other_time[0]]) == 1:
                    del teacher_busy_times_by_day[teacher][other_time[0]]
                else:
                    teacher_busy_times_by_day[teacher][other_time[0]].remove(other_time)
            cur_class, cur_time, other_time = mat_class, mat_time, pat_time

    def delta(self, schedule, moves, weights):
        x = self.get_objective_value_params(schedule, moves)
        new_score = self.objective(*x, weights)
        actual_new_score = self.objective(*x, (1, 1, 1))
        return x, actual_new_score, new_score - schedule.score

    def objective(self, gaps_count, single_bells_count, excess_days_count, class_super_impose_count, out_of_free_times_count, excess_lessons_count, weights=(1, 1)):
        objective = self.get_infeasibility_objective(weights, class_super_impose_count,
                                                     out_of_free_times_count)
        objective += self.get_gap_objective(gaps_count)
        objective += self.get_single_bell_objective(single_bells_count)
        objective += self.get_max_days_objective(excess_days_count)
        objective += self.get_excess_lessons_objective(excess_lessons_count)
        return objective

    def get_infeasibility_objective(self, weights, class_super_impose_count,
                                    out_of_free_times_count):
        objective = -self.infeasibility_imp * (
                weights[0] * class_super_impose_count + weights[1] * out_of_free_times_count)
        return objective

    def get_gap_objective(self, gaps_count):
        objective = -self.gap_imp * gaps_count
        return objective

    def get_single_bell_objective(self, single_bells_count):
        objective = -self.single_bell_imp * single_bells_count
        return objective

    def get_max_days_objective(self, excess_days_count):
        objective = -self.max_days_imp * excess_days_count
        return objective

    def get_excess_lessons_objective(self, excess_lessons_count):
        objective = -self.excess_lessons_imp * excess_lessons_count
        return objective

    def analyse(self, timetable):
        time_classes = {}
        teacher_busy_times_by_day = {}
        for teacher in timetable:
            for time in self.times:
                if timetable[teacher][time][1] != -1:
                    class_id = timetable[teacher][time][1]
                    time_classes[time][class_id] = time_classes.setdefault(time, {}).setdefault(class_id, 0) + 1
                    teacher_busy_times_by_day.setdefault(teacher, {}).setdefault(time[0], []).append(time)
        gaps_count = 0
        single_bells_count = 0
        excess_days_count = 0
        excess_lessons_count = 0
        for teacher in timetable:
            gaps_count += self.get_local_gaps_count(teacher_busy_times_by_day, teacher)
            single_bells_count += self.get_local_single_bells_count(teacher_busy_times_by_day, timetable, teacher)
            excess_days_count += self.get_local_excess_days_count(teacher_busy_times_by_day, teacher)
            excess_lessons_count += self.get_local_excess_lessons_count(timetable, teacher)
        excess_days_count = round(excess_days_count, 2)
        class_super_impose_count = sum(
            time_classes[time][class_id] - 1 for time in time_classes for class_id in time_classes[time])
        out_of_free_times_count = len(
            [time for teacher in teacher_busy_times_by_day for day in teacher_busy_times_by_day[teacher] for time in
             teacher_busy_times_by_day[teacher][day] if time not in self.teacher_free_times[teacher]])

        score = self.objective(gaps_count, single_bells_count, excess_days_count, class_super_impose_count, out_of_free_times_count, excess_lessons_count)

        return timetable, time_classes, teacher_busy_times_by_day, gaps_count, single_bells_count, excess_days_count, class_super_impose_count, out_of_free_times_count, score, excess_lessons_count

    def get_local_gaps_count(self, teacher_busy_times_by_day, teacher):
        if self.is_assistant[teacher]:
            return 0
        count = 0
        for day in teacher_busy_times_by_day[teacher]:
            last_time_slot = -1
            for time in sorted(teacher_busy_times_by_day[teacher][day]):
                if last_time_slot != -1:
                    count += time[1] - 1 - last_time_slot
                last_time_slot = time[1]
        return count

    def get_local_single_bells_count(self, teacher_busy_times_by_day, timetable, teacher):
        count = 0
        for day in teacher_busy_times_by_day[teacher]:
            for time in teacher_busy_times_by_day[teacher][day]:
                pair_time = self.get_pair(time)
                if pair_time is not None and timetable[teacher][pair_time][1] != timetable[teacher][time][1]:
                    count += 1
        return count

    def get_local_excess_days_count(self, teacher_busy_times_by_day, teacher):
        count = round(max(0, len(teacher_busy_times_by_day[teacher]) - self.teacher_max_days[teacher]) * self.teacher_max_days_penalize_weights[teacher], 2)
        return count

    def get_local_excess_lessons_count(self, timetable, teacher):
        count = 0
        for day in self.days:
            today_classes = {}
            for hour in self.hours:
                course, class_id = timetable[teacher][(day, hour)]
                if class_id != -1:
                    today_classes[(course, class_id)] = today_classes.setdefault((course, class_id), 0) + 1
            for item in today_classes:
                count += max(today_classes[item] - self.max_lessons_each_day, 0)
        return count

    def get_pair(self, time):
        if self.odd_bells_school and time[1] == len(self.hours) - 1:
            return None
        return (time[0], time[1] + 1) if time[1] % 2 == 0 else (time[0], time[1] - 1)

    def get_objective_value_params(self, schedule, moves):
        gaps_count = schedule.gaps_count
        single_bells_count = schedule.single_bells_count
        excess_days_count = schedule.excess_days_count
        excess_lessons_count = schedule.excess_lessons_count
        class_super_impose_count = schedule.class_super_impose_count
        out_of_free_times_count = schedule.out_of_free_times_count
        if moves is not None:
            if len(moves) == 2 and moves[0][0] == moves[1][0]:
                raise RuntimeError

            partial_time_classes = self.get_partial_time_classes(schedule.time_classes, moves)
            for teacher, pat_time, mat_time in moves:
                pat_class = schedule.timetable[teacher][pat_time][1]
                mat_class = schedule.timetable[teacher][mat_time][1]
                gaps_count += self.get_relative_gap_count(teacher, pat_time, mat_time, pat_class, mat_class, schedule.teacher_busy_times_by_day)
                single_bells_count += self.get_relative_single_bell_count(teacher, pat_time, mat_time, pat_class, mat_class, schedule.timetable)
                excess_days_count += self.get_relative_excess_days_count(teacher, pat_time, mat_time, pat_class, mat_class, schedule.teacher_busy_times_by_day)
                excess_lessons_count += self.get_relative_excess_lessons_count(teacher, pat_time, mat_time, pat_class, mat_class, schedule.timetable)
                class_super_impose_count += self.get_relative_super_impose_count(teacher, pat_time, mat_time, pat_class, mat_class, partial_time_classes)
                out_of_free_times_count += self.get_relative_out_of_free_times_count(teacher, pat_time, mat_time, pat_class, mat_class)
                self.update_time_classes(partial_time_classes, pat_time, mat_time, pat_class, mat_class)
            excess_days_count = round(excess_days_count, 2)
        return gaps_count, single_bells_count, excess_days_count, class_super_impose_count, out_of_free_times_count, excess_lessons_count

    def get_relative_gap_count(self, teacher, pat_time, mat_time, pat_class, mat_class, teacher_busy_times_by_day):
        if self.is_assistant[teacher] or (pat_class != -1 and mat_class != -1):
            return 0
        else:
            relative = 0
            cur_class, cur_time, prev_time = pat_class, pat_time, mat_time
            for i in range(2):
                if cur_class == -1:
                    if cur_time[0] in teacher_busy_times_by_day[teacher]:
                        last_smaller_hour, first_bigger_hour = get_extreme_big_small_values(teacher_busy_times_by_day[teacher][cur_time[0]], cur_time[1])
                        relative += get_relative_gap_amount_by_smaller_bigger_value(last_smaller_hour, first_bigger_hour, cur_time[1], True)

                    if prev_time[0] == cur_time[0]:
                        day_busy_times = teacher_busy_times_by_day[teacher][prev_time[0]] + [cur_time]
                    else:
                        day_busy_times = teacher_busy_times_by_day[teacher][prev_time[0]]
                    last_smaller_hour, first_bigger_hour = get_extreme_big_small_values(day_busy_times, prev_time[1])
                    relative += get_relative_gap_amount_by_smaller_bigger_value(last_smaller_hour, first_bigger_hour, prev_time[1], False)
                cur_class, cur_time, prev_time = mat_class, mat_time, pat_time
            # print("relative_gap_count", relative)
            return relative

    def get_relative_single_bell_count(self, teacher, pat_time, mat_time, pat_class, mat_class, timetable):
        if pat_class == mat_class:
            return 0
        else:
            relative = 0
            pat_pair = self.get_pair(pat_time)
            mat_pair = self.get_pair(mat_time)
            cur_time, cur_pair, cur_class, other_time, other_class = pat_time, pat_pair, pat_class, mat_time, mat_class
            if pat_pair != mat_time:
                for i in range(2):
                    if cur_pair is not None:
                        cur_pair_class = timetable[teacher][cur_pair][1]
                        relative += self.get_number_of_single_bells(cur_pair_class, other_class) - self.get_number_of_single_bells(cur_pair_class, cur_class)
                    cur_time, cur_pair, cur_class, other_time, other_class = mat_time, mat_pair, mat_class, pat_time, pat_class
            # print("relative single bell count", relative)
            return relative

    def get_number_of_single_bells(self, pat, mat):
        count = 0
        if pat != mat:
            if pat != -1:
                count += 1
            if mat != -1:
                count += 1
        return count

    def get_relative_excess_days_count(self, teacher, pat_time, mat_time, pat_class, mat_class, teacher_busy_times_by_day):
        if pat_class != -1 and mat_class != -1:
            return 0
        else:
            relative = 0
            cur_class, cur_time, prev_time = pat_class, pat_time, mat_time
            if pat_time[0] != mat_time[0]:
                for i in range(2):
                    if cur_class == -1:
                        if cur_time[0] not in teacher_busy_times_by_day[teacher]:
                            relative += 1
                        if len(teacher_busy_times_by_day[teacher][prev_time[0]]) == 1:
                            relative -= 1
                    cur_class, cur_time, prev_time = mat_class, mat_time, pat_time

            prev_busy_days_count = len(teacher_busy_times_by_day[teacher])
            pre_excess = max(0, prev_busy_days_count - self.teacher_max_days[teacher])
            cur_excess = max(0, prev_busy_days_count + relative - self.teacher_max_days[teacher])
            out = round((cur_excess - pre_excess) * self.teacher_max_days_penalize_weights[teacher], 2)
            return out

    def get_relative_excess_lessons_count(self, teacher, pat_time, mat_time, pat_class, mat_class, timetable):
        mat_item = timetable[teacher][mat_time]
        pat_item = timetable[teacher][pat_time]
        if pat_item == mat_item or pat_time[0] == mat_time[0]:
            return 0
        else:
            cur_item, cur_time, other_time = mat_item, mat_time, pat_time
            relative = 0
            for _ in range(2):
                if cur_item != (-1, -1):
                    cur_item_count_on_cur_day = 0
                    for hour in self.hours:
                        if timetable[teacher][(cur_time[0], hour)] == cur_item:
                            cur_item_count_on_cur_day += 1
                    if cur_item_count_on_cur_day == self.max_lessons_each_day + 1:
                        relative -= 1

                    cur_item_count_on_other_day = 0
                    for hour in self.hours:
                        if timetable[teacher][(other_time[0], hour)] == cur_item:
                            cur_item_count_on_other_day += 1
                    if cur_item_count_on_other_day == self.max_lessons_each_day:
                        relative += 1
                cur_item, cur_time, other_time = pat_item, pat_time, mat_time
            return relative

    def get_relative_super_impose_count(self, teacher, pat_time, mat_time, pat_class, mat_class, time_classes):
        if pat_class == mat_class:
            return 0
        else:
            relative = 0
            if mat_class != -1:
                if mat_class in time_classes[pat_time]:
                    relative += 1
                if time_classes[mat_time][mat_class] > 1:
                    relative -= 1
            if pat_class != -1:
                if pat_class in time_classes[mat_time]:
                    relative += 1
                if time_classes[pat_time][pat_class] > 1:
                    relative -= 1
            # print("relative_super impose", relative)
            return relative

    def get_relative_out_of_free_times_count(self, teacher, pat_time, mat_time, pat_class, mat_class):
        if pat_class != -1 and mat_class != -1:
            return 0
        cur_class, cur_time, prev_time = pat_class, pat_time, mat_time
        for i in range(2):
            if cur_class == -1:
                if self.is_in_free_times[teacher][cur_time]:
                    if not self.is_in_free_times[teacher][prev_time]:
                        return -1
                else:
                    if self.is_in_free_times[teacher][prev_time]:
                        return 1
            cur_class, cur_time, prev_time = mat_class, mat_time, pat_time
        return 0

    def get_partial_time_classes(self, full_time_classes, moves):
        p_time_classes = {}
        for _, pat_time, mat_time in moves:
            times = [pat_time, mat_time]
            for time in times:
                if time not in p_time_classes:
                    p_time_classes[time] = copy.copy(full_time_classes[time])
        return p_time_classes

    def update_time_classes(self, time_classes, pat_time, mat_time, pat_class, mat_class):
        cur_class, cur_time, other_time = pat_class, pat_time, mat_time
        for i in range(2):
            if cur_class != -1:
                time_classes[other_time][cur_class] = time_classes[other_time].setdefault(cur_class, 0) + 1
                if time_classes[cur_time][cur_class] == 1:
                    del time_classes[cur_time][cur_class]
                else:
                    time_classes[cur_time][cur_class] = time_classes[cur_time][cur_class] - 1
            cur_class, cur_time, other_time = mat_class, mat_time, pat_time

    def get_max_days_penalize_weights(self):
        max_day_penalize_weights = {}
        for teacher in self.teachers_itn:
            max_day_penalize_weights[teacher] = round(1 + (self.teacher_max_days[teacher] * len(self.hours) - self.param_provider.teacher_hours_count[teacher]) / len(self.hours), 2)
        return max_day_penalize_weights