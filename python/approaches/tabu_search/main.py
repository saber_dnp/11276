import time

from approaches.tabu_search.tabu_context import TabuContext
from i_o.import_zone import parser
from i_o.import_zone.csv_importer import CsvImporter
from i_o.import_zone.param_provider import ParamProvider

if __name__ == '__main__':
    opt = True
    if opt:
        start_time = time.time()
        param_provider = ParamProvider()
        parser.fill_from_file("../../i_o/import_zone/config_file_2", param_provider)
        print(param_provider.get_the_most_constraining_days_log())
        tabu_context = TabuContext(4, 1000000, 1000, (1, 1), 300, 350, (1.8, 2.2), 10, .4, 3, .4, 3, param_provider)
        csv_importer = CsvImporter(param_provider)
        # tabu_context.pass_by(csv_importer.import_csv("../../i_o/extract_zone/csv_files/final_output/schedule.csv"))
        tabu_context.pass_by()
        print("execution time: ", time.time() - start_time)
