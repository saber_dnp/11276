import copy
from itertools import combinations
from random import randint, sample, uniform

from approaches.tabu_search.analyser import Analyser
from approaches.tabu_search.schedule import Schedule
from i_o.extract_zone.extractor import Extractor



def count_zeros_and_non_zeros(lst):
    zero_count = 0
    non_zero_count = 0
    for item in lst:
        if item == 0:
            zero_count += 1
        else:
            non_zero_count += 1
    return zero_count, non_zero_count


class TabuContext:
    def __init__(self, cycles, RNA_max, TS_max, default_weights, min_tabu_tenure, max_tabu_tenure, dynamic_weight_coef_boundary, dynamic_weight_update_period, super_impose_min, super_impose_max, out_of_free_times_min, out_of_free_times_max, param_provider):
        self.teachers_courses = {}
        self.days, self.hours, self.times, self.courses_itn, self.teachers_itn, self.classes_itn, self.teacher_fields, self.teacher_free_times, self.teacher_job_hours, self.class_grades, self.class_curriculum, self.teacher_max_days, self.hour_spec, self.fields, self.is_assistant = param_provider.get_params()
        self.tabu_list = {}
        self.schedule = None
        self.cycles = cycles
        self.RNA_max = RNA_max
        self.TS_max = TS_max
        self.no_improve = 0
        self.idle_cycles = 0
        self.tabu_iteration = 0
        self.best_schedule = None
        self.default_weights = default_weights
        self.dynamic_weights = self.default_weights
        self.dynamic_weights_mins = (super_impose_min, out_of_free_times_min)
        self.dynamic_weights_maxs = (super_impose_max, out_of_free_times_max)
        self.dynamic_weights_update_period = dynamic_weight_update_period
        self.dynamic_weight_alteration_boundary = dynamic_weight_coef_boundary
        self.super_impose_infeasibility_log = []
        self.out_of_free_times_infeasibility_log = []
        self.min_tabu_tenure = min_tabu_tenure
        self.max_tabu_tenure = max_tabu_tenure
        # self.analyser = Analyser(5, 2, 1, 4, 4, 3, param_provider)
        self.analyser = Analyser(10, 0, 0, 0, 0, 0, param_provider)
        self.extractor = Extractor(param_provider)
        self.best_feasible_schedule = None

    def pass_by(self, timetable=None):
        self.deter_teachers_courses()
        self.initiate_schedule()
        # self.schedule = Schedule(self.analyser, *self.analyser.analyse(timetable))
        self.extractor.extract_txt(self.schedule)
        self.idle_cycles = 0
        while self.idle_cycles < self.cycles:
            self.idle_cycles += 1
            self.perform_rna()
            self.best_schedule = copy.deepcopy(self.schedule)
            self.perform_tabu_search()
            self.schedule = copy.deepcopy(self.best_schedule)
        # self.extractor.extract_final_edition()

    def perform_rna(self):
        print("rna started")
        self.no_improve = 0
        while self.no_improve < self.RNA_max:
            self.no_improve += 1
            double_move = self.random_double_move()
            analyse_outputs, delta = self.delta(double_move, self.default_weights)
            if delta >= 0:
                self.update_current_schedule(double_move, analyse_outputs)
                if delta > 0:
                    self.no_improve = 0
                    self.idle_cycles = 0

            print(self.schedule.score, self.schedule.gaps_count, self.schedule.single_bells_count, self.schedule.excess_days_count, self.schedule.class_super_impose_count, self.schedule.out_of_free_times_count, self.schedule.excess_lessons_count)

    def perform_tabu_search(self):
        print("tabu started")
        self.no_improve = 0
        self.tabu_iteration = 0
        self.tabu_list = {}
        self.super_impose_infeasibility_log = []
        self.out_of_free_times_infeasibility_log = []
        while self.no_improve < self.TS_max:
            print("no improve: ", self.no_improve)
            self.augment_feasibility_logs()
            moves = []
            for move in self.atomic_neighbourhood():
                analyse_outputs, delta = self.delta([move], self.dynamic_weights)
                if move not in self.tabu_list or self.aspired(analyse_outputs[5]):
                    moves.append((move, analyse_outputs, delta))
            best_move_stats = max(moves, key=lambda x: x[2])

            best_move = best_move_stats[0]
            best_move_analyse_outputs = best_move_stats[1]
            self.update_current_schedule([best_move], best_move_analyse_outputs)
            self.update_tabu_list(best_move)
            if self.is_feasible():
                if self.best_feasible_schedule is None or self.schedule.score > self.best_feasible_schedule.score:
                    self.best_feasible_schedule = copy.deepcopy(self.schedule)
                    self.extractor.extract_txt(copy.copy(self.best_feasible_schedule))
            if self.schedule.score > self.best_schedule.score:
                self.best_schedule = copy.deepcopy(self.schedule)
                self.no_improve = 0
                self.idle_cycles = 0
                print("tabu all time best recoreded ______________________________________________________")
            if self.is_update_iteration():
                self.update_dynamic_weights()
            self.tabu_iteration += 1
            self.no_improve += 1
            print(self.schedule.score, self.schedule.gaps_count, self.schedule.single_bells_count, self.schedule.excess_days_count, self.schedule.class_super_impose_count, self.schedule.out_of_free_times_count, self.schedule.excess_lessons_count)
            print(self.dynamic_weights)

    def is_feasible(self):
        return self.schedule.class_super_impose_count == 0 and self.schedule.out_of_free_times_count == 0
    """schedule initiation"""
    def deter_teachers_courses(self):
        preserved_courses = {}
        class_curriculum_remaining = {}
        for class_id in self.classes_itn:
            class_curriculum_remaining[class_id] = copy.deepcopy(
                self.class_curriculum[self.class_grades[class_id]])
        hour_spec_remaining = copy.deepcopy(self.hour_spec)

        for teacher_id in self.teachers_itn:
            teacher_special_courses = self.get_teacher_associated_courses(teacher_id, class_curriculum_remaining, hour_spec_remaining, True)
            preserved_courses[teacher_id] = teacher_special_courses
        for teacher_id in self.teachers_itn:
            courses = self.get_teacher_associated_courses(teacher_id, class_curriculum_remaining, hour_spec_remaining, False)
            courses.extend(preserved_courses[teacher_id])
            self.teachers_courses[teacher_id] = courses

    def get_teacher_associated_courses(self, teacher_id, class_curriculum_remaining, hour_spec_remaining, do_consider_special_courses=False):
        course_lists_took_care_of = []
        teacher_courses = []
        for course_list, hour_number in hour_spec_remaining[teacher_id].items():
            if not do_consider_special_courses or (
                    len(course_list) == 1 and self.courses_itn[course_list[0]] not in self.fields):
                courses = self.get_suitable_course_package(course_list, hour_number, teacher_id, class_curriculum_remaining)
                for _, course, class_ in courses:
                    class_curriculum_remaining[class_].pop(course)
                teacher_courses.extend(courses)
                course_lists_took_care_of.append(course_list)
        for course_list in course_lists_took_care_of:
            del hour_spec_remaining[teacher_id][course_list]
        return teacher_courses

    def get_suitable_course_package(self, course_list, hour, teacher_id, class_curriculum_remaining):
        eligible_courses = self.get_eligible_courses(course_list, class_curriculum_remaining)
        subsets = (
            subset for length in range(1, len(eligible_courses) + 1)
            for subset in combinations(eligible_courses, length)
        )
        hour_sum_correct_course_packages = [tuple(sorted(subset, reverse=True, key=lambda x: x[0])) for subset in
                                            subsets if
                                            sum([course[0] for course in subset]) == hour]
        hour_pattern = [course[0] for course in
                        max(hour_sum_correct_course_packages, key=lambda subset: [course[0] for course in subset])]
        hour_pattern_correct_packages = [package for package in hour_sum_correct_course_packages if
                                         [course[0] for course in package] == hour_pattern]
        return hour_pattern_correct_packages[randint(0, len(hour_pattern_correct_packages) - 1)]

    def get_eligible_courses(self, course_list, class_curriculum_remaining):
        return [(class_curriculum_remaining[class_id][course_id], course_id, class_id)
                for class_id in self.classes_itn for course_id in class_curriculum_remaining[class_id] if
                course_id in course_list]

    def generate_random_schedule(self):
        schedule = {}
        for teacher_id in self.teachers_itn:
            schedule[teacher_id] = {}
            for time in self.times:
                schedule[teacher_id][time] = (-1, -1)

        for teacher_id in self.teachers_itn:
            self.assign_course_to_time_slot(teacher_id, self.teachers_courses[teacher_id], schedule)
        return schedule

    def assign_course_to_time_slot(self, teacher_id, course_package, schedule):
        avail_times = copy.deepcopy(self.times)
        for hour_number, course, class_ in course_package:
            hours = sample(avail_times, hour_number)
            for hour in hours:
                schedule[teacher_id][hour] = (course, class_)
                avail_times.remove(hour)

    def initiate_schedule(self):
        self.schedule = Schedule(self.analyser, *self.analyser.analyse(self.generate_random_schedule()))

    def update_current_schedule(self, moves, analyse_outputs):
        self.schedule = self.analyser.get_transformed_schedule(self.schedule, moves, analyse_outputs)

    def random_double_move(self):
        moves = [self.random_atomic_move()]
        repair_move = self.get_repairer_move(moves[0])
        if repair_move is not None:
            moves.append(repair_move)
        return moves


    def get_repairer_move(self, move):
        teacher, pat_time, mat_time = move
        pat_class, mat_class = self.schedule.timetable[teacher][pat_time][1], self.schedule.timetable[teacher][mat_time][1]
        if pat_class == mat_class:
            return None
        else:
            if pat_class != -1 and pat_class in self.schedule.time_classes[mat_time]:
                for teacher_id in self.teachers_itn:
                    if self.schedule.timetable[teacher_id][mat_time][1] == pat_class:
                        times = copy.deepcopy(self.times)
                        times.remove(mat_time)
                        hour = sample(times, 1)[0]
                        if self.is_hour_pair_acceptable(teacher_id, [hour, mat_time]):
                            if mat_time > hour:
                                return (teacher_id, hour, mat_time)
                            else:
                                return (teacher_id, mat_time, hour)
                        else:
                            return None

            elif mat_class != -1 and mat_class in self.schedule.time_classes[pat_time]:
                for teacher_id in self.teachers_itn:
                    if self.schedule.timetable[teacher_id][pat_time][1] == mat_class:
                        times = copy.deepcopy(self.times)
                        times.remove(pat_time)
                        hour = sample(times, 1)[0]
                        if self.is_hour_pair_acceptable(teacher_id, [hour, pat_time]):
                            if pat_time > hour:
                                return (teacher_id, hour, pat_time)
                            else:
                                return (teacher_id, pat_time, hour)
                        else:
                            return None
            return None


    def is_hour_pair_acceptable(self, teacher, hours):
        return self.schedule.timetable[teacher][hours[0]] != self.schedule.timetable[teacher][hours[1]]

    def delta(self, moves, weights):
        x, score, delta = self.analyser.delta(self.schedule, moves, weights)
        gaps, single_bells, excess_days, super_impose, out_of_free_times, excess_lessons_count = x
        return (gaps, single_bells, excess_days, super_impose, out_of_free_times, score, excess_lessons_count), delta

    def random_atomic_move(self):
        teacher = randint(0, len(self.teachers_itn) - 1)
        while True:
            times = sample(self.times, 2)
            if self.is_hour_pair_acceptable(teacher, times):
                if times[1] > times[0]:
                    return (teacher, times[0], times[1])
                else:
                    return (teacher, times[1], times[0])

    def atomic_neighbourhood(self):
        return[(teacher, subset[0], subset[1]) for teacher in self.schedule.timetable for subset in combinations(self.times, 2) if self.is_hour_pair_acceptable(teacher, subset)]

    def aspired(self, score):
        return score > self.best_schedule.score

    def update_tabu_list(self, move):
        new_move_tabu_applied = False
        new_move_tabu_duration = randint(self.min_tabu_tenure, self.max_tabu_tenure - 1)
        expired_tabus = []
        for tabu_move, remaining_time in self.tabu_list.items():
            self.tabu_list[tabu_move] = self.tabu_list[tabu_move] - 1
            if not new_move_tabu_applied and tabu_move == move:
                self.tabu_list[tabu_move] = self.tabu_list[tabu_move] + new_move_tabu_duration
                new_move_tabu_applied = True
            if self.tabu_list[tabu_move] == 0:
                expired_tabus.append(tabu_move)
        for tabu_move in expired_tabus:
            del self.tabu_list[tabu_move]
        if not new_move_tabu_applied:
            self.tabu_list[move] = new_move_tabu_duration

    def is_update_iteration(self):
        return self.tabu_iteration % self.dynamic_weights_update_period == 0 and self.tabu_iteration != 0

    def update_dynamic_weights(self):
        logs = [self.super_impose_infeasibility_log, self.out_of_free_times_infeasibility_log]
        new_weights = []
        for i in range(len(logs)):
            log = logs[i]
            zero_count, non_zero_count = count_zeros_and_non_zeros(log)
            weight = self.dynamic_weights[i]
            alteration_ratio = uniform(self.dynamic_weight_alteration_boundary[0], self.dynamic_weight_alteration_boundary[1])
            if non_zero_count == 0:
                weight = max(self.dynamic_weights_mins[i], weight / alteration_ratio)
            elif zero_count == 0:
                weight = min(self.dynamic_weights_maxs[i], weight * alteration_ratio)
            new_weights.append(round(weight, 2))

        self.dynamic_weights = tuple(new_weights)
        self.super_impose_infeasibility_log = []
        self.out_of_free_times_infeasibility_log = []

    def augment_feasibility_logs(self):
        self.super_impose_infeasibility_log.append(self.schedule.class_super_impose_count)
        self.out_of_free_times_infeasibility_log.append(self.schedule.out_of_free_times_count)
