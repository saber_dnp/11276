class Schedule:
    def __init__(self, analyser, timetable, time_classes, teacher_busy_times_by_day, gaps_count, single_bells_count, excess_days_count, class_super_impose_count, out_of_free_times_count, score, excess_lessons_count):
        self.timetable = timetable
        self.time_classes = time_classes
        self.teacher_busy_times_by_day = teacher_busy_times_by_day
        self.gaps_count = gaps_count
        self.single_bells_count = single_bells_count
        self.excess_days_count = excess_days_count
        self.excess_lessons_count = excess_lessons_count
        self.class_super_impose_count = class_super_impose_count
        self.out_of_free_times_count = out_of_free_times_count
        self.score = score
        self.analyser = analyser

    def get_gaps_count(self, teacher_id):
        return self.analyser.get_local_gaps_count(self.teacher_busy_times_by_day, teacher_id)

    def get_excess_days_count(self, teacher_id):
        return self.analyser.get_local_excess_days_count(self.teacher_busy_times_by_day, teacher_id)

    def get_excess_lessons_count(self, teacher_id):
        return self.analyser.get_local_excess_lessons_count(self.timetable, teacher_id)
