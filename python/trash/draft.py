from selenium import webdriver


def extract_to_html(s1, s2, s3, s4):
    browser = webdriver.Firefox()
    browser.get('file:///home/saber/PycharmProjects/11276_gitlab/cpp/schedule.html')

    # teacherName.concat(" Schedule with ".concat("<span class=\"small\">").concat(statistics).concat("</span>"))
    table_id = "teachers-table"
    table_caption = "Teachers Schedules"
    table_data = s1
    browser.execute_script("createTable(arguments[0], arguments[1], arguments[2], arguments[3])", table_data,
                           table_caption, table_id, "false")
    table_id = "classes-table"
    table_caption = "Classes Schedules"
    table_data = s2
    browser.execute_script("createTable(arguments[0], arguments[1], arguments[2], arguments[3])", table_data,
                           table_caption, table_id, "false")
    cnt = 0
    for teacher in teacher_name:
        table_id = teacher
        table_caption = table_id + " Schedule"
        table_data = s3[cnt]
        browser.execute_script("createTable(arguments[0], arguments[1], arguments[2], arguments[3])", table_data,
                               table_caption, table_id, "true")
        cnt += 1

    cnt = 0
    for class_ in class_name:
        table_id = class_
        table_caption = table_id + " Schedule"
        table_data = s4[cnt]
        browser.execute_script("createTable(arguments[0], arguments[1], arguments[2], arguments[3])", table_data,
                               table_caption, table_id, "false")
        cnt += 1
    browser.execute_script("addFooter()")
    f = open("/home/saber/PycharmProjects/11276_gitlab/cpp/new_html.html", "w")
    f.write(browser.page_source)
    f.close()


teacher_cnt = int(input())
class_cnt = int(input())
class_name = []
teacher_name = []
for i in range(teacher_cnt):
    teacher_name.append(input())
for i in range(class_cnt):
    class_name.append(input())

s1 = input()
s2 = input()
s3 = []
s4 = []
for i in range(teacher_cnt):
    s = input()
    s3.append(s)
for i in range(class_cnt):
    s = input()
    s4.append(s)
extract_to_html(s1, s2, s3, s4)
