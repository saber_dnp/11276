import itertools
from random import randint, uniform

import numpy as np

from evaluate.evaluate import Evaluator
from trash.genetic import DNA
from trash.output_extractor import Extractor


class ScoreNotValidException(Exception):
    pass


class Genetic:
    def __init__(self, DNA_number, mutate_prob, days, hours, courses, teachers, class_itn,
                 teacher_fields, teacher_free_times, teacher_job_hours,
                 class_grades, class_curriculums, teacher_max_days, hour_spec):
        self.days = days
        self.hours = hours
        self.times = self.get_product(self.days, self.hours)
        self.courses_itn = courses
        self.teachers_itn = teachers
        self.teacher_fields = teacher_fields
        self.teacher_job_hours = teacher_job_hours
        self.teacher_max_days = teacher_max_days
        self.teacher_hour_spec = hour_spec
        self.class_grades = class_grades
        self.class_curriculum = class_curriculums
        self.class_ids = [item[0] for item in self.class_grades.items()]
        self.teacher_ids = [item[0] for item in self.teachers_itn.items()]
        self.course_ids = [item[0] for item in self.courses_itn.items()]
        if teacher_free_times is None:
            self.teacher_free_times = {}
            for teacher_id in self.teacher_ids:
                self.teacher_free_times[teacher_id] = self.times
        else:
            self.teacher_free_times = teacher_free_times

        self.class_itn = class_itn
        self.scores = []
        self.break_point = len(self.times) * int(len(self.class_ids) / 2)

        self.DNAs = []
        self.DNA_number = DNA_number
        self.best_DNA = None
        self.mutate_prob = mutate_prob
        self.max_score = self.get_max_score()

        self.domains = {}
        for var, domain in self.get_domains():
            self.domains[var] = domain
        self.all_vars = [item[0] for item in self.domains.items()]
        self.true_courses = [np.array([item[1] for item in self.class_curriculum[self.class_grades[class_id]].items()])
                             for class_id in self.class_ids]
        self.true_spec_hours = [np.array([item[1] for item in self.teacher_hour_spec[teacher_id].items()]) for
                                teacher_id in self.teacher_ids]
        self.var_by_time = {}
        for time in self.times:
            self.var_by_time[time] = self.get_vars(time=time)
        self.var_by_class = {}
        for class_id in self.class_ids:
            self.var_by_class[class_id] = self.get_vars(class_id=class_id)

        self.solution_version = 0
        self.final_best_scores = []
        self.evaluator = Evaluator(self.class_ids, self.teacher_ids, self.class_curriculum, self.class_grades,
                                   self.teacher_hour_spec, self.times, self.teacher_max_days, self.true_courses,
                                   self.true_spec_hours, self.days, self.course_ids)
        self.evaluator.init_criteria()
        self.extractor = Extractor(self.teacher_ids, self.class_ids, self.hours, self.days, self.courses_itn,
                                   self.teachers_itn, self.class_itn, self.evaluator)

    def get_max_score(self):
        score = 0
        for class_id in self.class_ids:
            for item in self.class_curriculum[self.class_grades[class_id]].items():
                score += item[1] % 2
        return -score

    def pass_by(self):
        self.init_DNAs()
        print("max_score is :", self.max_score)
        while True:
            if self.best_DNA is not None and self.best_DNA.score >= self.max_score:
                break
            if self.best_DNA is not None and self.best_DNA.score > self.max_score * 5 and self.best_DNA.score not in self.final_best_scores:
                self.extractor.extract(self.best_DNA)
                self.final_best_scores.append(self.best_DNA.score)
            self.evolve()
        self.extractor.extract(self.best_DNA)

    def evolve(self):
        self.select()
        self.print_scores()
        self.cross_over()
        self.mutate()
        self.print_scores()

    def select(self):
        DNAs = []
        self.evaluate_()
        for _ in range(self.DNA_number - 1 if self.best_DNA is not None else 0):
            DNAs.append(self.opt_for_a_DNA())
        if self.best_DNA is not None:
            for _ in range(1):
                DNAs.append(self.best_DNA)
        self.DNAs = DNAs
        self.DNAs.sort(key=lambda x: x.score)

    def cross_over(self):
        dnas = []
        for i in range(0, self.DNA_number, 2):
            if i >= self.DNA_number - 1:
                dnas.append(self.DNAs[i])
                break
            parent1, parent2 = self.DNAs[i], self.DNAs[i + 1]
            child1, child2 = DNA(), DNA()
            for j in range(len(parent1.values)):
                var_name = self.all_vars[j]
                if j < self.break_point:
                    child1.values[var_name] = parent1.values[var_name]
                    child2.values[var_name] = parent2.values[var_name]
                else:
                    child1.values[var_name] = parent2.values[var_name]
                    child2.values[var_name] = parent1.values[var_name]
            dnas.append(child1)
            dnas.append(child2)
        self.DNAs = dnas

    def mutate(self):
        for dna in self.DNAs:
            if uniform(0, 1) < self.mutate_prob:
                var_to_be_modified = self.all_vars[randint(0, len(self.all_vars) - 1)]
                options_for_domain = self.domains[var_to_be_modified]
                dna.values[var_to_be_modified] = options_for_domain[randint(0, len(options_for_domain) - 1)]
                dna.score = -1

    def evaluate_(self):
        for dna in self.DNAs:
            dna.score = self.evaluator.fitness(dna)
            if self.best_DNA is None or dna.score > self.best_DNA.score:
                self.best_DNA = dna

    def opt_for_a_DNA(self):
        score_sum = 0
        offset = -min([dna.score for dna in self.DNAs]) + 20
        for dna in self.DNAs:
            if dna.score == -1:
                raise ScoreNotValidException
            score_sum += dna.score + offset

        rand = uniform(0, score_sum)
        score_sum = 0
        for dna in self.DNAs:
            score_sum += dna.score + offset
            if score_sum >= rand:
                return dna
        msg = "can not happen"
        raise RuntimeError(msg)

    def init_DNAs(self):
        for _ in range(self.DNA_number):
            self.DNAs.append(self.generate_random_DNA())
        print("DNAs inited")

    def generate_random_DNA(self):
        dna = DNA()
        for var in self.all_vars:
            domain = self.domains[var]
            dna.values[var] = domain[randint(0, len(domain) - 1)]
        return dna

    def print_scores(self):
        print(int(self.best_DNA.score))

    def get_vars(self, class_id=None, time=None):
        if time is None:
            if class_id is None:
                return [self.get_var_name(class_id, time) for class_id in self.class_ids for time in self.times]
            else:
                return [self.get_var_name(class_id, time) for time in self.times]
        else:
            if class_id is None:
                return [self.get_var_name(class_id, time) for class_id in self.class_ids]
            else:
                return [self.get_var_name(class_id, time)]

    def get_domains(self):
        return [(self.get_var_name(class_id, time), self.get_domain(class_id, time)) for class_id in self.class_ids for
                time in self.times]

    def get_product(self, *vars_):
        return [item for item in itertools.product(*vars_)]

    def get_var_name(self, class_id, time):
        return str(class_id) + "_" + str(time)

    def get_domain(self, class_id, time):
        return [(item[0], item[1], time) for item in self.get_general_domain() if
                time in self.teacher_free_times[item[1]] and item[0] in self.class_curriculum[
                    self.class_grades[class_id]]]

    def get_general_domain(self):
        return [item for item in self.get_product(self.course_ids, self.teacher_ids) if
                item[0] in self.teacher_fields[item[1]]]
