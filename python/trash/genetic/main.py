from i_o import parser
from trash.genetic.Genetic import Genetic
from i_o.param_provider import ParamProvider

if __name__ == '__main__':
    opt = True
    if opt:
        param_pro = ParamProvider()
        parser.fill_from_file("../../i_o/config_file", param_pro)
        x = param_pro.get_params()
        genetic = Genetic(20, .6, *x)
        # genetic.pass_by()
    else:
        y = [[0, 1], [0, 1, 2], {0: "math", 1: "arabic", 2: "varzesh"}, {0: "a", 1: "b", 2: "c", 3: "d"},
             {0: "class_1", 1: "class_2"},
             {0: [0, 1, 2], 1: [0, 1, 2], 2: [0, 1, 2], 3: [0, 1, 2]}, None, {0: 3, 1: 3, 2: 3, 3: 3},
             {0: "first", 1: "first"}, {"first": {0: 3, 1: 2, 2: 1}}, {0: 1, 1: 1, 2: 1, 3: 1},
             {0: {tuple([0]): 3}, 1: {tuple([1, 2]): 3}, 2: {tuple([0]): 3}, 3: {tuple([1, 2]): 3}}]
        genetic = Genetic(200, .8, *y)
        genetic.pass_by()
