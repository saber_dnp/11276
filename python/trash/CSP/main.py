from trash.CSP import Schedule


if __name__ == '__main__':
    # param_provider = ParamProvider()
    # file_to_param_pro.fill_from_file("config_file", param_provider)
    # x = param_provider.get_params()
    # schedule = Schedule(*x)
    # schedule.solve()
    y = [[0, 1], [0, 1, 2], {0: "math", 1: "arabic", 2: "varzesh"}, {0: "a", 1: "b", 2: "c", 3: "d"},
         {0: [0, 1, 2], 1: [0, 1, 2], 2: [0, 1, 2], 3: [0, 1, 2]}, None, {0: 3, 1: 3, 2: 3, 3: 3},
         {0: "first", 1: "first"}, {"first": {0: 3, 1: 2, 2: 1}}, {0: 1, 1: 1, 2: 1, 3: 1},
         {0: {tuple([0]): 3}, 1: {tuple([1, 2]): 3}, 2: {tuple([0]): 3}, 3: {tuple([1, 2]): 3}}]
    schedule = Schedule(*y)
    schedule.solve()



