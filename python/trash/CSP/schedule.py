import itertools

from constraint import Problem

from trash.genetic import Genetic

week_days = {0: "Saturday", 1: "Sunday", 2: "Monday", 3: "Tuesday", 4: "Wednesday", 5: "Thursday",
             6: "Friday"}


class Schedule:

    def print_curriculum_for(self, class_id, solution):
        print("class ", class_id)
        print("days\\ hours", end="   ")
        for i in range(len(self.hours)):
            print(str(i + 1), end="      ")
        print()
        course_teachers = [item[1] for item in
                           sorted([item for item in solution.items() if int(item[0].split("_")[0]) == class_id])]
        for i in range(len(self.days)):
            print(week_days[i], end="   ")
            for j in range(len(self.hours)):
                print(self.courses[course_teachers[i * len(self.hours) + j][0]],
                      self.teachers[course_teachers[i * len(self.hours) + j][1]], end="  ")
            print()
        print("____________")

    def print_solution(self):
        solution = self.problem.getSolution()
        for class_id in self.class_ids:
            self.print_curriculum_for(class_id, solution)

    def get_product(self, *vars_):
        return [item for item in itertools.product(*vars_)]

    def get_general_domain(self):
        return [item for item in self.get_product(self.course_ids, self.teacher_ids) if
                item[0] in self.teacher_fields[item[1]]]

    def get_domain(self, class_id, time):
        return [(item[0], item[1], time) for item in self.get_general_domain() if
                time in self.teacher_free_times[item[1]] and item[0] in self.class_curriculums[
                    self.class_grades[class_id]]]

    def get_var_name(self, class_id, time):
        return str(class_id) + "_" + str(time)

    def get_vars(self, class_id=None, time=None):
        if time is None:
            if class_id is None:
                return [self.get_var_name(class_id, time) for class_id in self.class_ids for time in self.times]
            else:
                return [self.get_var_name(class_id, time) for time in self.times]
        else:
            if class_id is None:
                return [self.get_var_name(class_id, time) for class_id in self.class_ids]
            else:
                return [self.get_var_name(class_id, time)]

    def make_1teacher_1time_2places_lambda(self, pat, mat):
        return lambda x, y: x[1] != y[1], [pat, mat]

    def add_1teacher_1time_2places_consts(self):
        for time in self.times:
            variables = self.get_vars(time=time)
            for const in [(self.make_1teacher_1time_2places_lambda(variables[i], variables[j])) for i in
                          range(len(variables)) for j in
                          range(i + 1, len(variables))]:
                self.problem.addConstraint(const[0], const[1])

    def make_curriculum_lambda(self, class_id, course_id):
        return lambda *x: len([var for var in x if var[0] == course_id]) == self.class_curriculums[self.class_grades[class_id]][course_id]

    def add_curriculum_const(self):
        for class_id in self.class_ids:
            for course_id in self.class_curriculums[self.class_grades[class_id]].keys():
                self.problem.addConstraint(self.make_curriculum_lambda(class_id, course_id), self.get_vars(class_id=class_id))

    def make_teacher_hour_lambda(self, teacher_id):
        return lambda *x: len([var[1] for var in x if var[1] == teacher_id]) == self.teacher_job_hours[teacher_id]

    def add_teacher_hour_const(self):
        for teacher_id in self.teacher_ids:
            self.problem.addConstraint(self.make_teacher_hour_lambda(teacher_id), self.all_vars)

    def make_hour_spec_lambda(self, teacher_id, course, hour):
        return lambda *x: len([var for var in x if var[1] == teacher_id and var[0] in course]) == hour

    def add_hour_spec_const(self):
        variables = self.all_vars
        for teacher_id in self.teacher_ids:
            for const in [(self.make_hour_spec_lambda(teacher_id, course, hour)) for course, hour in
                          self.hour_specific[teacher_id].items()]:
                self.problem.addConstraint(const, variables)

    def make_teacher_max_days_lambda(self, teacher_id):
        return lambda *x: len(set([var[2][0] for var in x if var[1] == teacher_id])) <= self.teacher_max_days[teacher_id]

    def add_teacher_max_days_const(self):
        variables = self.all_vars
        for teacher_id in self.teacher_ids:
            self.problem.addConstraint(self.make_teacher_max_days_lambda(teacher_id), variables)

    def make_1class_1course_2teachers_lambda(self, pat, mat):
        return lambda x, y: x[0] != y[0] or x[1] == y[1], [pat, mat]

    def add_1class_1course_2teachers_consts(self):
        for class_id in self.class_ids:
            variables = self.get_vars(class_id=class_id)
            for const in [(self.make_1class_1course_2teachers_lambda(variables[i], variables[j])) for i in
                          range(len(variables)) for j in range(i + 1, len(variables))]:
                self.problem.addConstraint(const[0], const[1])

    def make_single_bell_lambda(self, course):
        def bell_const(*x):
            course_vars = [var for var in x if var[0] == course]
            alone_count = 0
            for var in course_vars:
                corresponding = self.get_corresponding(var[2])
                if len([var2 for var2 in course_vars if var2[2] == corresponding]) == 0:
                    alone_count += 1
                    if alone_count > self.single_bell_tolerance:
                        return False
            return True

        return bell_const

    def add_single_bell_const(self):
        for class_id in self.class_ids:
            variables = self.get_vars(class_id=class_id)
            for const in [self.make_single_bell_lambda(course) for course in
                          set(self.class_curriculums[self.class_grades[class_id]])]:
                self.problem.addConstraint(const, variables)

    def make_teacher_schedule_gap_lambda(self, teacher_id, day):
        return lambda *x: self.is_consicuitive(
            sorted([var[2][1] for var in x if var[1] == teacher_id and var[2][0] == day]))

    def add_teacher_schedule_gap_const(self):
        variables = self.all_vars
        for teacher_id in self.teacher_ids:
            for const in [(self.make_teacher_schedule_gap_lambda(teacher_id, day)) for day in self.days]:
                self.problem.addConstraint(const, variables)

    def get_corresponding(self, time):
        return (time[0], time[1] + 1) if time[1] % 2 == 0 else (time[0], time[1] - 1)

    def is_consicuitive(self, _list):
        if len(_list) == 0:
            return True
        return abs(sum(_list) - (_list[-1] - _list[0] + 1) * (_list[0] + _list[-1]) / 2) < .01

    def add_constraints(self):
        self.add_1teacher_1time_2places_consts()
        self.add_curriculum_const()
        self.add_teacher_hour_const()
        self.add_hour_spec_const()
        self.add_teacher_max_days_const()
        self.add_1class_1course_2teachers_consts()
        self.add_single_bell_const()
        self.add_teacher_schedule_gap_const()

    def add_variables(self):
        for class_id in self.class_ids:
            for time in self.times:
                self.problem.addVariable(self.get_var_name(class_id, time), self.get_domain(class_id, time))

    def __init__(self, days, hours, courses, teachers, teacher_fields, teacher_free_times, teacher_job_hours,
                 class_grades, class_curriculums, teacher_max_days, hour_specific):
        self.genetic = Genetic(20, .2, days, hours, courses, teachers, teacher_fields,
                               teacher_free_times, teacher_job_hours, class_grades, class_curriculums,
                               teacher_max_days, hour_specific)
        self.days = days
        self.hours = hours
        self.times = self.get_product(self.days, self.hours)

        self.courses = courses
        self.course_ids = [item[0] for item in self.courses.items()]

        self.teachers = teachers
        self.teacher_ids = [item[0] for item in self.teachers.items()]
        self.teacher_fields = teacher_fields
        self.teacher_job_hours = teacher_job_hours
        self.teacher_max_days = teacher_max_days
        self.hour_specific = hour_specific

        if teacher_free_times is None:
            self.teacher_free_times = {}
            for teacher_id in self.teacher_ids:
                self.teacher_free_times[teacher_id] = self.times
        else:
            self.teacher_free_times = teacher_free_times

        self.class_grades = class_grades
        self.class_curriculums = class_curriculums
        self.class_ids = [item[0] for item in self.class_grades.items()]
        self.problem = Problem()
        self.all_vars = self.get_vars()

        self.single_bell_tolerance = 1

    def solve(self):
        self.add_variables()
        self.add_constraints()
        self.print_solution()
