import random

from trash.informed_genetics.DNA import DNA


class Crosser:

    def __init__(self, pc, evaluator):
        self.pc = pc
        self.evaluator = evaluator

    def cross_over(self, dnas):
        new_dnas = []
        teacher_count = len(dnas[0].schedule)
        for i in range(0, len(dnas), 2):
            if i >= len(dnas) - 1:
                new_dnas.append(dnas[i])
                break
            if random.random() < self.pc:
                parent1, parent2 = dnas[i], dnas[i + 1]
                best_teachers_parent1 = self.get_best_teachers(parent1.schedule)
                child1, child2 = DNA(), DNA()
                # break_point = random.randint(0, teacher_count)
                break_point = int(teacher_count / 2)
                for i in range(break_point):
                    child1.schedule[best_teachers_parent1[i]] = parent1.schedule[best_teachers_parent1[i]]
                    child2.schedule[best_teachers_parent1[i]] = parent2.schedule[best_teachers_parent1[i]]
                for i in range(break_point, teacher_count):
                    child1.schedule[best_teachers_parent1[i]] = parent2.schedule[best_teachers_parent1[i]]
                    child2.schedule[best_teachers_parent1[i]] = parent1.schedule[best_teachers_parent1[i]]
                new_dnas.append(child1)
                new_dnas.append(child2)
            else:
                new_dnas.append(dnas[i])
                new_dnas.append(dnas[i + 1])
        return new_dnas

    def get_best_teachers(self, schedule):
        return [item[0] for item in sorted([(teacher_id, self.evaluator.get_local_objective_value(schedule[teacher_id], teacher_id)) for teacher_id in schedule], key=lambda x: x[1], reverse=True)]

