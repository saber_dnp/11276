from trash.informed_genetics import Genetic
from i_o import parser
from i_o.param_provider import ParamProvider
import time

if __name__ == '__main__':
    opt = True
    if opt:
        start_time = time.time()
        param_provider = ParamProvider()
        parser.fill_from_file("../../i_o/config_file", param_provider)
        x = param_provider.get_params()
        genetic = Genetic(20, 3, 1, 1, 1, .8, .02, .01, .3, 0, 3, *x)
        genetic.pass_by(100000)
        print("execution time: ", time.time() - start_time)