import copy
from itertools import combinations
from random import randint

from trash.informed_genetics.DNA import DNA


class SuggestedHourNotInTeacherFreeTimes(Exception):
    pass


class Initiator:

    def __init__(self, DNA_count, evaluator, *x):
        self.days, self.hours, self.times, self.courses_itn, self.teachers_itn, self.classes_itn, self.teacher_fields, self.teacher_free_times, self.teacher_job_hours, self.class_grades, self.class_curriculum, self.teacher_max_days, self.hour_spec, self.fields = x
        self.teacher_queue = self.get_teachers_by_priority()
        self.teachers_courses = {}
        self.DNA_count = DNA_count
        self.evaluator = evaluator

    def get_DNAs(self):
        self.deter_teachers_courses()
        dnas = []
        for _ in range(self.DNA_count):
            dna = DNA()
            schedule = self.generate_feasible_schedule()
            dna.schedule = schedule
            dnas.append(dna)
            print("one inited")
        return dnas

    def deter_teachers_courses(self):
        preserved_courses = {}
        class_curriculum_remaining = {}
        for class_id in self.classes_itn:
            class_curriculum_remaining[class_id] = copy.deepcopy(
                self.class_curriculum[self.class_grades[class_id]])
        hour_spec_remaining = copy.deepcopy(self.hour_spec)

        for teacher_id in self.teacher_queue:
            teacher_special_courses = self.get_teacher_associated_courses(teacher_id, class_curriculum_remaining, hour_spec_remaining, True)
            preserved_courses[teacher_id] = teacher_special_courses
        for teacher_id in self.teacher_queue:
            courses = self.get_teacher_associated_courses(teacher_id, class_curriculum_remaining, hour_spec_remaining, False)
            courses.extend(preserved_courses[teacher_id])
            self.teachers_courses[teacher_id] = courses

    def get_teacher_associated_courses(self, teacher_id, class_curriculum_remaining, hour_spec_remaining, do_consider_special_courses=False):
        course_lists_took_care_of = []
        teacher_courses = []
        for course_list, hour_number in hour_spec_remaining[teacher_id].items():
            if not do_consider_special_courses or (
                    len(course_list) == 1 and self.courses_itn[course_list[0]] not in self.fields):
                courses = self.get_suitable_course_package(course_list, hour_number, teacher_id, class_curriculum_remaining)
                for _, course, class_ in courses:
                    class_curriculum_remaining[class_].pop(course)
                teacher_courses.extend(courses)
                course_lists_took_care_of.append(course_list)
        for course_list in course_lists_took_care_of:
            del hour_spec_remaining[teacher_id][course_list]
        return teacher_courses

    def get_suitable_course_package(self, course_list, hour, teacher_id, class_curriculum_remaining):
        eligible_courses = self.get_eligible_courses(course_list, class_curriculum_remaining)
        subsets = (
            subset for length in range(1, len(eligible_courses) + 1)
            for subset in combinations(eligible_courses, length)
        )
        hour_sum_correct_course_packages = [tuple(sorted(subset, reverse=True, key=lambda x: x[0])) for subset in
                                            subsets if
                                            sum([course[0] for course in subset]) == hour]
        hour_pattern = [course[0] for course in
                        max(hour_sum_correct_course_packages, key=lambda subset: [course[0] for course in subset])]
        hour_pattern_correct_packages = [package for package in hour_sum_correct_course_packages if
                                         [course[0] for course in package] == hour_pattern]
        return hour_pattern_correct_packages[randint(0, len(hour_pattern_correct_packages) - 1)]

    def get_eligible_courses(self, course_list, class_curriculum_remaining):
        return [(class_curriculum_remaining[class_id][course_id], course_id, class_id)
                for class_id in self.classes_itn for course_id in class_curriculum_remaining[class_id] if
                course_id in course_list]

    def generate_feasible_schedule(self):
        schedule = {}
        for teacher_id in self.teachers_itn:
            schedule[teacher_id] = {}
            for time in self.times:
                schedule[teacher_id][time] = (-1, -1)

        teacher_times = copy.deepcopy(self.teacher_free_times)
        for teacher_id in self.teacher_queue:
            self.assign_course_to_time_slot(teacher_id, self.teachers_courses[teacher_id], schedule, teacher_times)
        self.evaluator.solve_conflicts(schedule)
        return schedule

    def assign_course_to_time_slot(self, teacher_id, course_package, schedule, teacher_times):
        for hour_number, course, class_ in course_package:
            hours = self.get_best_hours(teacher_id, class_, schedule, teacher_times)
            for i in range(hour_number):
                schedule[teacher_id][hours[i]] = (course, class_)
                if hours[i] in teacher_times[teacher_id]:
                    teacher_times[teacher_id].remove(hours[i])
                else:
                    raise SuggestedHourNotInTeacherFreeTimes(
                        "hour " + hours[i] + " not in " + self.teachers_itn[teacher_id] + " free times")

    def get_best_hours(self, teacher_id, class_to_teach, schedule, teacher_times):
        return [item[1] for item in sorted([(len([schedule[teacher][time][1] for teacher in self.teachers_itn if
                                                  schedule[teacher][time][1] == class_to_teach]), time) for time in
                                            teacher_times[teacher_id]], key=lambda x: x[0])]


    def get_teachers_by_priority(self):
        teachers = []
        for teacher_id in self.teachers_itn:
            free_time_len = len(self.teacher_free_times[teacher_id])
            work_len = self.teacher_job_hours[teacher_id]
            teachers.append((free_time_len / (work_len + .0001), teacher_id))
        out = [item[1] for item in sorted(teachers, key=lambda x: x[0])]
        return out
