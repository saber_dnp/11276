from random import uniform


class ScoreNotValidException(Exception):
    pass


class Selector:

    def __init__(self, evaluator):
        self.evaluator = evaluator

    def select(self, dnas):
        selected_dnas = []
        for dna in dnas:
            dna.score = self.evaluator.objective(dna.schedule)
        # print(sorted([dna.score for dna in dnas]))
        for _ in range(len(dnas)):
            selected_dnas.append(self.opt_for_a_dna(dnas))
        selected_dnas.sort(key=lambda x: x.score)
        return selected_dnas, selected_dnas[len(selected_dnas) - 1]

    def opt_for_a_dna(self, dnas):
        score_sum = 0
        objective_values = [dna.score for dna in dnas]
        min_objective_value = min(objective_values)
        max_objective_value = max(objective_values)
        scores = []
        for dna in dnas:
            if dna.score > 0:
                raise ScoreNotValidException
            score = self.evaluator.fitness(dna.score, min_objective_value, max_objective_value)
            scores.append(score)
            score_sum += score
        # print(sorted(scores))

        rand = uniform(0, score_sum)
        score_sum = 0
        for i in range(len(scores)):
            score = scores[i]
            score_sum += score
            if score_sum >= rand:
                return dnas[i]
        raise RuntimeError("can not happen")
