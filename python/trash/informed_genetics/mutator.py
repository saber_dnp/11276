import random


def get_random_teacher(dna):
    teachers = [teacher for teacher in dna.schedule]
    return teachers[random.randint(0, len(teachers) - 1)]


class MisIntervalException(Exception):
    pass


class Mutator:

    def __init__(self, pmd, pm1, pmk, k, days, hours, times):
        self.pmd = pmd
        self.pm1 = pm1
        self.pmk = pmk
        self.k = k
        self.days = days
        self.hours = hours
        self.times = times

    def mutate(self, dnas):
        for dna in dnas:
            if random.random() < self.pmd:
                self.day_mutate(dna)
            if random.random() < self.pm1:
                self.one_mutate(dna)
            if random.random() < self.pmk:
                self.k_mutate(dna)

    def day_mutate(self, dna):
        teacher = get_random_teacher(dna)
        days = random.sample(self.days, 2)
        for hour in self.hours:
            temp = dna.schedule[teacher][(days[0], hour)]
            dna.schedule[teacher][(days[0], hour)] = dna.schedule[teacher][(days[1], hour)]
            dna.schedule[teacher][(days[1], hour)] = temp

    def one_mutate(self, dna):
        teacher = get_random_teacher(dna)
        times = random.sample(self.times, 2)
        temp = dna.schedule[teacher][times[0]]
        dna.schedule[teacher][times[0]] = dna.schedule[teacher][times[1]]
        dna.schedule[teacher][times[1]] = temp

    def k_mutate(self, dna):
        teacher = get_random_teacher(dna)
        pat_start = random.randint(0, len(self.times) - 2 * self.k)
        mat_start = random.randint(pat_start + self.k, len(self.times) - self.k)
        if mat_start < pat_start + self.k or mat_start + self.k > len(self.times) or pat_start + self.k > len(self.times):
            raise MisIntervalException("pat start: " + str(pat_start) + " mat start: " + mat_start)

        for i in range(self.k):
            pat_time = self.times[pat_start + i]
            mat_time = self.times[mat_start + i]
            temp = dna.schedule[teacher][pat_time]
            dna.schedule[teacher][pat_time] = dna.schedule[teacher][mat_time]
            dna.schedule[teacher][mat_time] = temp

