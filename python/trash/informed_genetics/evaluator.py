from random import shuffle, randint


def get_pair(time):
    return (time[0], time[1] + 1) if time[1] % 2 == 0 else (time[0], time[1] - 1)


class ScheduleConcatException(Exception):
    pass


class Evaluator:
    def __init__(self, infeasibility_imp, gap_imp, single_bell_imp, max_days_imp, max_infeasibility_count, *x):
        self.days, self.hours, self.times, self.courses_itn, self.teachers_itn, self.classes_itn, self.teacher_fields, self.teacher_free_times, self.teacher_job_hours, self.class_grades, self.class_curriculum, self.teacher_max_days, self.hour_spec, self.fields = x
        self.infeasibility_imp, self.gap_imp, self.single_bell_imp, self.max_days_imp = infeasibility_imp, gap_imp, single_bell_imp, max_days_imp
        self.max_infeasiblity_count = max_infeasibility_count

    """ evaluation part """
    def objective(self, schedule):
        total_objective_value = 0
        for teacher_id in schedule:
            total_objective_value += self.get_local_objective_value(schedule[teacher_id], teacher_id)
        total_objective_value += self.get_infeasibility_objective_value(schedule)
        return total_objective_value

    def fitness(self, objective_value, min_objective_value, max_objective_value):
        return self.convert_objective_to_fitness(objective_value, min_objective_value, max_objective_value)

    def get_infeasibility_objective_value(self, schedule):
        objective = -self.infeasibility_imp * self.get_number_of_conflicts(schedule)
        return objective

    def get_local_objective_value(self, teacher_schedule, teacher_id):
        return self.gap_imp * self.get_gap_objective_value(teacher_schedule, teacher_id) + self.single_bell_imp * self.get_single_bell_objective_value(teacher_schedule, teacher_id) + self.max_days_imp * self.get_max_days_objective_value(teacher_schedule, teacher_id)

    def get_gap_objective_value(self, teacher_schedule, teacher_id):
        last_day = -1
        last_time_slot = -1
        objective_value = 0
        for time in sorted([time for time in teacher_schedule if teacher_schedule[time][0] != -1]):
            if time[0] == last_day:
                gap_len = (time[1] - last_time_slot - 1)
                objective_value += gap_len * (2 ** gap_len)
            last_time_slot = time[1]
            last_day = time[0]
        out = -objective_value
        # print(self.teachers_itn[teacher_id], " gap objective: ", out)
        return out

    def get_single_bell_objective_value(self, teacher_schedule, teacher_id):
        objective_value = 0
        for time in [time for time in teacher_schedule if teacher_schedule[time][1] != -1]:
            pair_time = get_pair(time)
            if pair_time in self.teacher_free_times[teacher_id] and teacher_schedule[pair_time][1] != teacher_schedule[time][1] and teacher_schedule[pair_time][1] != -1:
                objective_value += 1
        out = -objective_value
        # print(self.teachers_itn[teacher_id], " single_bell objective :", out)
        return out

    def get_max_days_objective_value(self, teacher_schedule, teacher_id):
        excess_days_num = max(len(set([time[0] for time in teacher_schedule if teacher_schedule[time][0] != -1])) - self.teacher_max_days[teacher_id], 0)
        out = -excess_days_num * (2 ** excess_days_num)
        # print(self.teachers_itn[teacher_id], " max days objective", out)
        # print("_____________________")
        return out

    def convert_objective_to_fitness(self, objective_value, min_objective_value, max_objective_value):
        # return (objective_value - min_objective_value) / (max_objective_value - min_objective_value + .0001) * 80 + 20
        return objective_value + (-min_objective_value + 20)

    """ conflict resolution part """

    def solve_conflicts(self, schedule):
        tweak_count = 0
        for i in range(len(schedule)):
            partial_schedule = {key: schedule[key] for key in range(i + 1)}
            partial_schedule, tweak_number = self.solve_partial_schedule(partial_schedule)
            tweak_count += tweak_number
            for teacher_id, value in partial_schedule.items():
                schedule[teacher_id] = value
        if self.get_number_of_conflicts(schedule) > self.max_infeasiblity_count:
            raise ScheduleConcatException
        print("all conflicts resolved with " + str(tweak_count) + " tweaks")

    def solve_partial_schedule(self, partial_schedule):
        over, miss = self.get_over_and_miss(partial_schedule)
        tweak_number = 0
        while True:
            for i in range(3):
                tweak_number += self.side_way_move([key for key in partial_schedule][randint(0, len(partial_schedule) - 1)], over, miss, partial_schedule)
            for teacher_id in partial_schedule:
                conflict_num = sum([sum([len([partial_schedule[teacher_id][time][1] for teacher_id in partial_schedule if partial_schedule[teacher_id][time][1] == class_]) - 1 for class_ in over[time]]) for time in self.times])
                if conflict_num <= self.max_infeasiblity_count:
                    return partial_schedule, tweak_number
                tweak_number += self.swap_with_blank(teacher_id, over, miss, partial_schedule)

    def get_over_and_miss(self, schedule):
        over, miss, covered_classes = {}, {}, {}
        for time in self.times:
            over[time] = []
            miss[time] = []
            covered_classes[time] = []
        for teacher_id in schedule:
            for time in self.times:
                class_id = schedule[teacher_id][time][1]
                if class_id == -1:
                    continue
                if class_id not in covered_classes[time]:
                    covered_classes[time].append(class_id)
                else:
                    if class_id not in over[time]:
                        over[time].append(class_id)
        for time, classes in covered_classes.items():
            for class_id in self.classes_itn:
                if class_id not in classes:
                    miss[time].append(class_id)
        return over, miss

    def classic_swap(self, teacher_id, over, miss, schedule):
        overed_items = []
        for time in self.times:
            class_id = schedule[teacher_id][time][1]
            if class_id != -1 and class_id in over[time]:
                overed_items.append((class_id, time))
        if len(overed_items) > 1:
            for i in range(len(overed_items)):
                for j in range(i + 1, len(overed_items)):
                    pat = overed_items[i]
                    mat = overed_items[j]
                    pat_time = pat[1]
                    pat_class = pat[0]
                    mat_time = mat[1]
                    mat_class = mat[0]
                    if pat_class in miss[mat_time] and mat_class in miss[mat_time]:
                        temp = schedule[teacher_id][pat_time]
                        schedule[teacher_id][pat_time] = schedule[teacher_id][mat_time]
                        schedule[teacher_id][mat_time] = temp
                        miss[pat_time].remove(mat_class)
                        miss[mat_time].remove(pat_class)
                        items = [pat, mat]
                        for item in items:
                            count = 0
                            for teacher_id in schedule:
                                if schedule[teacher_id][item[1]][1] == item[0]:
                                    count += 1
                            if count <= 1:
                                over[item[1]].remove(item[0])
                        print("one classic swap made")
                        return 1
        return 0

    def swap_with_blank(self, teacher_id, over, miss, schedule):
        overed_items = []
        for time in self.times:
            class_id = schedule[teacher_id][time][1]
            if class_id != -1 and class_id in over[time]:
                overed_items.append((class_id, time))
        tweak_number = 0
        for over_item in overed_items:
            if over_item[0] in over[over_item[1]]:
                tweak_number += self.fix_by_swap_with_blank(teacher_id, over, miss, over_item, schedule)
        return tweak_number

    def fix_by_swap_with_blank(self, teacher_id, over, miss, over_item, schedule):
        for time, classes in miss.items():
            if schedule[teacher_id][time][0] == -1 and over_item[0] in classes and time in self.teacher_free_times[teacher_id]:
                schedule[teacher_id][time] = schedule[teacher_id][over_item[1]]
                schedule[teacher_id][over_item[1]] = (-1, -1)
                miss[time].remove(over_item[0])
                count = 0
                for teacher_id in schedule:
                    if schedule[teacher_id][over_item[1]][1] == over_item[0]:
                        count += 1
                if count <= 1:
                    over[over_item[1]].remove(over_item[0])
                return 1
        return 0

    def classic_chain_swap(self, teacher_id, over, miss, schedule):
        pass

    def side_way_move(self, teacher_id, over, miss, schedule):
        shuffle(self.teacher_free_times[teacher_id])
        for time in self.teacher_free_times[teacher_id]:
            class_ = schedule[teacher_id][time][1]
            if class_ != -1 and class_ not in over[time]:
                for candidate_time in self.teacher_free_times[teacher_id]:
                    if schedule[teacher_id][candidate_time][1] == -1 and class_ in miss[candidate_time]:
                        schedule[teacher_id][candidate_time] = schedule[teacher_id][time]
                        schedule[teacher_id][time] = (-1, -1)
                        miss[candidate_time].remove(class_)
                        miss[time].append(class_)
                        return 1
        return 0

    def get_number_of_conflicts(self, schedule):
        conflicts = 0
        for time in self.times:
            lst = [schedule[teacher_id][time][1] for teacher_id in schedule if
                   schedule[teacher_id][time][1] != -1]
            new_conflicts = len(lst) - len(set(lst))
            conflicts += new_conflicts
        return conflicts

    def recover_partial_feasibility(self, dnas):
        for dna in dnas:
            if self.get_number_of_conflicts(dna.schedule) > self.max_infeasiblity_count:
                self.solve_conflicts(dna.schedule)