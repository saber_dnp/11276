import copy

from trash.informed_genetics.crosser import Crosser
from trash.informed_genetics import Initiator
from trash.informed_genetics.local_search import LocalSearcher
from trash.informed_genetics.mutator import Mutator
from trash.informed_genetics.selector import Selector
from trash.informed_genetics.evaluator import Evaluator
from i_o.extract_zone.txt_extractor import TxtExtractor


class Genetic:
    def __init__(self, DNA_count, infeasiblity_imp, gap_imp, single_bell_imp, max_days_imp, pc, pmd, pmk, pm1,
                 max_infeasiblity_count, k, *x):
        self.days, self.hours, self.times, self.courses_itn, self.teachers_itn, self.classes_itn, self.teacher_fields, self.teacher_free_times, self.teacher_job_hours, self.class_grades, self.class_curriculum, self.teacher_max_days, self.hour_spec, self.fields = x
        self.extractor = TxtExtractor(*x)
        self.evaluator = Evaluator(infeasiblity_imp, gap_imp, single_bell_imp, max_days_imp, max_infeasiblity_count, *x)
        self.initiator = Initiator(DNA_count, self.evaluator, *x)
        self.selector = Selector(self.evaluator)
        self.crosser = Crosser(pc, self.evaluator)
        self.mutator = Mutator(pmd, pm1, pmk, k, self.days, self.hours, self.times)
        self.local_searcher = LocalSearcher()
        self.best_dna = None
        self.DNA_count = DNA_count
        self.DNAs = []
        self.pc = pc
        self.pm3 = pmk
        self.pm1 = pm1
        self.pmd = pmd
        self.turn = 0

    def pass_by(self, step_number):
        self.init_dnas()
        for _ in range(step_number):
            self.evolve()

    def init_dnas(self):
        self.DNAs = self.initiator.get_DNAs()
        print("dnas initiated")

    def evolve(self):
        self.select()
        print([dna.score for dna in self.DNAs])
        print("________")
        self.cross_over()
        self.mutate()
        self.local_search()
        # self.recover_feasibility()
        self.turn += 1

    def select(self):
        self.DNAs, best_dna = self.selector.select(self.DNAs)
        if self.best_dna is None or best_dna.score > self.best_dna.score:
            self.best_dna = copy.deepcopy(best_dna)

    def cross_over(self):
        self.DNAs = self.crosser.cross_over(self.DNAs)

    def mutate(self):
        self.mutator.mutate(self.DNAs)

    def local_search(self):
        self.local_searcher.local_search(self.DNAs)

    def recover_feasibility(self):
        self.evaluator.recover_partial_feasibility(self.DNAs)
