from i_o.utils.table import Table


class Extractor:
    def __init__(self, teacher_ids, class_ids, hours, days, courses_itn, teachers_itn, class_itn, evaluator):
        self.solution_version = 0
        self.teacher_ids = teacher_ids
        self.class_ids = class_ids
        self.evaluator = evaluator
        self.hours = hours
        self.days = days
        self.courses_itn = courses_itn
        self.teachers_itn = teachers_itn
        self.class_itn = class_itn
        self.week_days = {0: "Saturday", 1: "Sunday", 2: "Monday", 3: "Tuesday", 4: "Wednesday", 5: "Thursday",
                          6: "Friday"}

    def extract(self, dna):
        f = open("schedule" + str(self.solution_version), "w")
        table = Table(f)
        self.print_schedule_whole(dna, table)
        table.separate()
        for class_id in self.class_ids:
            self.print_schedule_for_class(class_id, dna, table)
        table.separate()
        for teacher_id in self.teacher_ids:
            self.print_schedule_for_teacher(teacher_id, dna, table)
        table.separate()
        self.evaluator.fitness(dna, table)
        f.close()
        self.solution_version += 1

    def print_schedule_for_class(self, class_id, answer_dna, table):
        table.put_into_next_line("class {}".format(self.class_itn[class_id]))
        table.put_into_next_line("days\\ hours")
        for hour in self.hours:
            table.put_into_next_column(str(hour + 1))
        course_teachers = [item[1] for item in sorted(
            [item for item in answer_dna.values.items() if int(item[0].split("_")[0]) == class_id])]
        for day in self.days:
            table.put_into_next_line(self.week_days[day])
            for hour in self.hours:
                table.put_into_next_column(self.courses_itn[course_teachers[day * len(self.hours) + hour][0]] + "/" +
                                           self.teachers_itn[course_teachers[day * len(self.hours) + hour][1]])
        table.partial_separate()

    def print_schedule_for_teacher(self, teacher_id, dna, table):
        table.put_into_next_line("teacher schedule :{}".format(self.teachers_itn[teacher_id]))
        table.put_into_next_line("days\\ hours")
        for hour in self.hours:
            table.put_into_next_column(str(hour + 1))
        teacher_schedule = [var[1][2] for var in dna.values.items() if var[1][1] == teacher_id]
        for day in self.days:
            table.put_into_next_line(self.week_days[day])
            for hour in self.hours:
                if (day, hour) in teacher_schedule:
                    table.put_into_next_column(u'\u2714')
                else:
                    table.put_into_next_column(u'\u2716')
        table.partial_separate()

    def print_schedule_whole(self, dna, table):
        table.put_into_next_line("schedule score is: {}".format(dna.score))
        table.put_into_next_line("days\\ hours")
        for day in self.days:
            for hour in self.hours:
                table.put_into_next_column(self.week_days[day] + ", " + str(hour + 1))
        for class_id in self.class_ids:
            table.put_into_next_line(self.class_itn[class_id])
            for day in self.days:
                for hour in self.hours:
                    course_id, teacher_id, _ = dna.values[self.get_var_name(class_id, (day, hour))]
                    table.put_into_next_column(self.courses_itn[course_id] + "/" + self.teachers_itn[teacher_id])

    def get_var_name(self, class_id, time):
        return str(class_id) + "_" + str(time)
