// In god we trust
 
#include <bits/stdc++.h>
#include <chrono>
#include <algorithm>
#include <signal.h>
#include <thread>
#include <pthread.h>
#include <omp.h>


#define pb push_back
#define fi first
#define se second
#define Vi vector<int>
#define Vb vector<bool>
#define Pi pair<int, int>
#define MP make_pair
#define MT make_tuple
#define Q queue
#define Qi Q<int>
#define INF 1000000000 
#define LINF 1000000000000000000
using namespace std;
using namespace std::chrono;


unsigned seed = 0;

const int MAX_T = 100; // max_teacher_cnt
const int MAX_H = 50; // max_hour_cnt
const int MAX_R = 150; // max_course_cnt
const int MAX_C = 50; // max_class_cnt
const int MAX_D = 7; // max_days_cnt
const int MAX_HOURS_EACH_DAY = 15; // max hours each day
const int MAX_B = 50; // max base cnt
int T; // teacher_cnt
int H; // weekly_hour_cnt
int R; // course cnt
int C; // class_cnt
int D; // day cnt
int HOURS_EACH_DAY;
int B; // base cnt
vector<Pi> v[MAX_T]; // teacher_(course, class)_pairs assigned to
int h[MAX_T]; // how many hours does teacher i teach
int b[MAX_B]; // how many classes are there for base i
int teacher_max_days[MAX_T]; // max days for teacher i 
unordered_map<int, int> curriculum[MAX_B]; // cnt of hours needed for each course of base i
int which_base[MAX_C]; // which base does class i belong to
string time_[MAX_H];
string course[MAX_R]; // course names
string teacher[MAX_T]; // teacher names
string base[MAX_B]; // base names
string class_[MAX_C]; // class names
unordered_map<string, int> r_string_to_num; // converter of course_name to course_num
unordered_map<string, int> t_string_to_num; // converter of tacher_name to teacher_num
unordered_map<string, int> c_string_to_num; // converter of class_name to class_num
unordered_map<string, int> b_string_to_num; // converter of base_name to base_num
unordered_map<string, int> field[MAX_T]; // cnt of hours specific for fields of teacher i
vector<pair<Vi, int>> hour_spec[MAX_T]; // (course_list, hour) pairs for teacher i
set<string> course_name_set;
bool is_assistant[MAX_T];
vector<Vi> table; // class that teacher i goes on hour j
vector<Vi> course_table; // course that teacher i teaches on hour j 
vector<Vi> td; // how many hours does teacher i teach in day j
vector<Vi> cd; // sum of units that are tought to calss i in day j
vector<Vi> hc; // cnt of class j in timeslot i
vector<int> h_; // how many hours each day is
bool is_free[MAX_T][MAX_H]; // is teacher i free on timeslot j
int idle_cycles = 0, idle = 0;
int tabu[MAX_T][MAX_H][MAX_H];
int MAX_CYCLES = 4;
int RNA_MAX = 1000000;
int TS_MAX = 10000;
double cur_obj; // current objective
int now = 0;
vector<Vi> BEST; //best table found
vector<Vi> BEST_COURSE_TABLE; // best solution's course table
double BEST_OBJ = INF; // best solutin's objective
double BEST_F_OBJ = INF; // best feasible solution's objective
int TENURE_MIN = 10, TENURE_MAX = 25;
int K = 10; // period of updating the dynamic weights
double W = 2.5; // weight of infeasibility
int OW_I = 1; // initial OW
int SW_I = 1; // initial SW
double OW = OW_I; // out of free time objective weight
double SW = SW_I ; // super impose objective weight
bool LOG_O_SET = 1; // all out of free times are 1
bool LOG_O_RESET = 1; // all out of free times are 0
bool LOG_S_SET = 1; // all super imposes are 1
bool LOG_S_RESET = 1; // all super imposes are 0
double O_ALPHA_MIN = 1.8; // out of free time update factor min, e.g. 1.8
double O_ALPHA_MAX = 2.2; // out of free time update factor max, e.g. 2.2
double S_ALPHA_MIN = 1.8; // super impose update factor min
double S_ALPHA_MAX = 2.2; // super impose update factor max
double OW_MIN = .4;
double OW_MAX = 3;
double SW_MIN = .4;
double SW_MAX = 3;
double GW = 1.1; // gap weight
double SBW = 1.2; // single bell weight
int DW = 3; // days weight
double SUW = .05; // square unit sum weight
int su_sum_lower_bound; // a lower bound calculated for square unit sum for all classes aggregated
vector<pair<vector<Vi>, vector<Vi>>> feasible_schedules;

// soft constraints
int g_cnt; // cur schedule gap cnt
int sb_cnt; // cur schedule single bell cnt
int md_cnt; // cur schedule max days cnt (number of violations of max days)
int su_sum; // sum of squares of units of classes in each day
// hard constraints 
int o_cnt; // cur schedule out of free time cnt 
int s_cnt; // cur schedule super impose cnt
unordered_map<int, string> day;

int cursor = 0;
const int COLUMN_WIDTH = 30;


int proc_cnt;

void revert_to_best();
void evaluate_schedule();


void normal_column(fstream &fout, string s){
    fout << s << ",";
}

void last_column(fstream &fout, string s){
    fout << s << "\n";
}


// h: how many hours is each day
void export_to_csv(string file_name){
  vector<int> h = h_;
  fstream fout;
  fout.open("./csv_files/" + file_name, ios::out);
  normal_column(fout, "teachers");
  int cur_day = 0;
  int cur_hour = 0;
  int next_threshold = h[cur_day];
  for (int i = 0; i < table[0].size(); i++){
    if (i == table[0].size() - 1)
      last_column(fout, day[cur_day] + "/" + to_string(cur_hour));
    else
      normal_column(fout, day[cur_day] + "/" + to_string(cur_hour));
    cur_hour++;
    if (i == next_threshold)
      cur_day++, cur_hour = 0, next_threshold += h[cur_day];
  }
  for (int i = 0; i < table.size(); i++) {
    normal_column(fout, teacher[i]);
    for (int j = 0; j < table[i].size(); j++) {
      if (j == table[i].size() - 1)
	last_column(fout, to_string(table[i][j]) + "/" + to_string(course_table[i][j]));
      else
	normal_column(fout, to_string(table[i][j]) + "/" + to_string(course_table[i][j]));
    }
  }
}

string encode_teachers_table(){
  string s = "Teacher&";
  for (int i = 0; i < H; i++)
    s += time_[i] + "&";
  s = s.substr(0, s.size() - 1);
  s += "^";
  for (int i = 0; i < T; i++){
    s += teacher[i] + "&";
    for (int j = 0; j < H; j++)
      if (table[i][j] != -1)
	s += class_[table[i][j]] + "/" + course[course_table[i][j]] + "&";
      else
	s += " &";
    s = s.substr(0, s.size() - 1);
    s += "^";
  }
  s = s.substr(0, s.size() - 1);
  return s;
}


string encode_classes_table(vector<Vi> class_schedule, vector<Vi> class_course_schedule){
  string s = "Class&";
  for (int i = 0; i < H; i++)
    s += time_[i] + "&";
  s = s.substr(0, s.size() - 1);
  s += "^";
  for (int i = 0; i < C; i++){
    s += class_[i] + "&";
    for (int j = 0; j < H; j++)
      s += teacher[class_schedule[i][j]] + "/" + course[class_course_schedule[i][j]] + "&";
    s = s.substr(0, s.size() - 1);
    s += "^";
  }
  s = s.substr(0, s.size() - 1);
  return s;
}

string encode_specific_teacher_table(int t){
  string s = "Day&";
  for (int i = 0; i < HOURS_EACH_DAY; i++)
    s += to_string(i + 1) + "&";
  s = s.substr(0, s.size() - 1);
  s += "^";
  for (int i = 0; i < D; i++){
    s += day[i] + "&";
    for (int j = 0; j < HOURS_EACH_DAY; j++)
      if (table[t][i * HOURS_EACH_DAY + j] != -1)
	s += class_[table[t][i * HOURS_EACH_DAY + j]] + "/" + course[course_table[t][i * HOURS_EACH_DAY + j]] + "&";
      else
	s += " &";
    s = s.substr(0, s.size() - 1);
    s += "^";
  }
  s = s.substr(0, s.size() - 1);
  return s;
}

string encode_specific_class_table(Vi this_class_schedule, Vi this_class_course_schedule){
  string s = "Day&";
  for (int i = 0; i < HOURS_EACH_DAY; i++)
    s += to_string(i + 1) + "&";
  s = s.substr(0, s.size() - 1);
  s += "^";
  for (int i = 0; i < D; i++){
    s += day[i] + "&";
    for (int j = 0; j < HOURS_EACH_DAY; j++)
      s += teacher[this_class_schedule[i * HOURS_EACH_DAY + j]] + "/" + course[this_class_course_schedule[i * HOURS_EACH_DAY + j]] + "&";
    s = s.substr(0, s.size() - 1);
    s += "^";
  }
  s = s.substr(0, s.size() - 1);
  return s;
}

void export_to_html(){
  vector<Vi> class_schedule; // teacher of class i in time j
  vector<Vi> class_course_schedule; // course of class i in time j

  // initializing both to -1
  for (int i = 0; i < C; i++){
    Vi new_vec;
    for (int j = 0; j < H; j++)
      new_vec.pb(-1);
    class_schedule.pb(new_vec);
  }
  for (int i = 0; i < C; i++){
    Vi new_vec;
    for (int j = 0; j < H; j++)
      new_vec.pb(-1);
    class_course_schedule.pb(new_vec);
  }
  
  for (int i = 0; i < T; i++)
    for (int j = 0; j < H; j++)
      if (table[i][j] != -1)
	class_schedule[table[i][j]][j] = i, class_course_schedule[table[i][j]][j] = course_table[i][j];
  
  fstream fout;
  fout.open("./js_data", ios::out | ios::app);
  fout << T << endl;
  fout << C << endl;
  for (int i = 0; i < T; i++)
    fout << teacher[i] << endl;
  for (int i = 0; i < C; i++)
    fout << class_[i] << endl;
  
  fout << encode_teachers_table() << endl;
  fout << encode_classes_table(class_schedule, class_course_schedule) << endl;
  for (int i = 0; i < T; i++)
    fout << encode_specific_teacher_table(i) << endl;
  for (int i = 0; i < C; i++)
    fout << encode_specific_class_table(class_schedule[i], class_course_schedule[i]) << endl;
  fout.close();
  system("python3 html_exporter.py < js_data");
  system("rm -rf js_data");
}


//returns the table and course_table
pair<vector<vector<int>>, vector<vector<int>>> import_from_csv(string file_name){
  fstream fin;
  vector<vector<int>> table;
  vector<vector<int>> course_table;
  fin.open("./csv_files/" + file_name, ios::in);
  string line, word, temp;

  int cnt = 0;
  bool first_row = true;
  while (fin >> temp) {
    vector<int> classes;
    vector<int> courses;
    if (first_row){
      first_row = false;
      continue;
    }
    stringstream s(temp);

    bool first_word_seen = false;
    while (getline(s, word, ',')) {
      if (not first_word_seen){
	first_word_seen = true;
	continue;
      }
      string class_;
      string course_;
      bool seen_slash = false;
      for (int i = 0; i < word.size(); i++)
	if (word[i] == '/')
	  seen_slash = true;
	else{
	  if (not seen_slash)
	    class_ += word[i];
	  else
	    course_ += word[i];
	}
      classes.push_back(stoi(class_));
      courses.push_back(stoi(course_));
    }
    table.push_back(classes);
    course_table.push_back(courses);
  }
  return make_pair(table, course_table);
}


int rand(int min, int max){
  return rand()%(max-min + 1) + min;
}

double rand_double(double min, double max){
  return ((double) rand()*(max-min)/(double)RAND_MAX) + min;
}


//TODO:[NOTE] this should be changed for schools with HOURS_EACH_DAY != 7
int get_comp(int a){
  int mod = a % HOURS_EACH_DAY;
  if (mod == HOURS_EACH_DAY - 1)
    return -1;
  if ((a & 1) ^ ((a / 7) & 1))
    return a - 1;
  else
    return a + 1;
}


// true if s1 is substring of s2 and false otherwise 
bool is_substring(string s1, string s2) 
{ 
    int M = s1.length(); 
    int N = s2.length(); 
  
    for (int i = 0; i <= N - M; i++) { 
        int j; 
        for (j = 0; j < M; j++) 
            if (s2[i + j] != s1[j]) 
                break; 
        if (j == M) 
            return true; 
    } 
    return false; 
}

int get_teacher_gap_cnt(int i){
  int sum = 0;
  Vi vec;
  for (int j = 0; j < H; j++)
    if (table[i][j] != -1)
      vec.pb(j);
  int pre = -1;
  for (auto item: vec){
    if (pre != -1)
      sum += (item - pre - 1) * (pre / HOURS_EACH_DAY == item / HOURS_EACH_DAY);
    pre = item;
  }
  return (!is_assistant[i]) * sum;
}

int get_teacher_single_bell_cnt(int i){
  int sum = 0;
  int c = -1;
  for (int j = 0; j < H; j++){
    if (j == c)
      continue;
    c = get_comp(j); // complement hour of j
    if (c != -1 and table[i][c] != table[i][j])
      sum++;
  }
  return sum;
}

int get_teacher_max_day_cnt(int i){
  set<int> s; // set of days that this teacher teaches in 
  for (int j = 0; j < H; j++)
    if (table[i][j] != -1)
      s.insert(j / HOURS_EACH_DAY);
  return max(0, (int) s.size() - teacher_max_days[i]);
}



void justify(ostream& stream){
  while(cursor % COLUMN_WIDTH){
    stream << " ";
    cursor++;
  }
}

void next_column(ostream& stream, string s){
  stream << s;
  cursor += s.size();
  justify(stream);
}



void next_line(ostream& stream, string s){
  stream << "\n";
  cursor = 0;
  stream << s;
  cursor += s.size();
  justify(stream);
}


void print_schedule(string file_name){
  fstream fout;
  fout.open("./txt_files/" + file_name, ios::out);
  
  next_line(fout, to_string(g_cnt) + " " + to_string(sb_cnt) + " " + to_string(md_cnt) + " " + to_string(su_sum) + " " + to_string(o_cnt) + " " + to_string(s_cnt) + " total objective: " + to_string(cur_obj) + "  with weights: " + to_string(GW) + " " + to_string(SBW) + " " + to_string(DW) + " " + to_string(SUW) + " " + to_string(W * OW) + " " + to_string(W * SW));
  next_line(fout, "teachers");
  for (int i = 0; i < H; i++)
    next_column(fout, day[i / HOURS_EACH_DAY] + " " + to_string(i % HOURS_EACH_DAY));
  next_column(fout, "gap");
  next_column(fout, "single_bell");
  next_column(fout, "excess days");
  for (int i = 0; i < T; i++){
    next_line(fout, teacher[i]);
    for (int j = 0; j < H; j++)
      if (table[i][j] != -1)
	next_column(fout, class_[table[i][j]] +  "/ " + course[course_table[i][j]]);
      else
	next_column(fout, " ");
    next_column(fout, to_string(get_teacher_gap_cnt(i)));
    next_column(fout, to_string(get_teacher_single_bell_cnt(i)));
    next_column(fout, to_string(get_teacher_max_day_cnt(i)));
  }
  next_line(fout, "super imposes");
  for (int i = 0; i < H; i++){
    int sum = 0;
    for (int j = 0; j < C; j++)
      sum += max(0, hc[i][j] - 1);
    next_column(fout, to_string(sum));
  }
  next_line(fout, "");
  cursor = 0;
}

void print_schedule(){
  next_line(cout, to_string(g_cnt) + " " + to_string(sb_cnt) + " " + to_string(md_cnt) + " " + to_string(su_sum) + " " + to_string(o_cnt) + " " + to_string(s_cnt) + " total objective: " + to_string(cur_obj) + "  with weights: " + to_string(GW) + " " + to_string(SBW) + " " + to_string(DW) + " " + to_string(SUW) + " " + to_string(W * OW) + " " + to_string(W * SW));
  next_line(cout, "teachers");
  for (int i = 0; i < H; i++)
    next_column(cout, day[i / HOURS_EACH_DAY] + " " + to_string(i % HOURS_EACH_DAY));
  next_column(cout, "gap");
  next_column(cout, "single_bell");
  next_column(cout, "excess days");
  for (int i = 0; i < T; i++){
    next_line(cout, teacher[i]);
    for (int j = 0; j < H; j++)
      if (table[i][j] != -1)
	next_column(cout, class_[table[i][j]] +  "/ " + course[course_table[i][j]]);
      else
	next_column(cout, " ");
    next_column(cout, to_string(get_teacher_gap_cnt(i)));
    next_column(cout, to_string(get_teacher_single_bell_cnt(i)));
    next_column(cout, to_string(get_teacher_max_day_cnt(i)));
  }
  next_line(cout, "super imposes");
  for (int i = 0; i < H; i++){
    int sum = 0;
    for (int j = 0; j < C; j++)
      sum += max(0, hc[i][j] - 1);
    next_column(cout, to_string(sum));
  }
  next_line(cout, "");
  cursor = 0;
}

void signal_callback_handler(int signum){
  int a;
  cout << "caught signal " << signum << endl;
  cout << "please enter the schedle index you prefer" << endl;
  cin >> a;
  table = feasible_schedules[a].fi, course_table = feasible_schedules[a].se;
  evaluate_schedule();
  export_to_csv("final_schedule.csv");
  export_to_html();
  exit(0);
}


int get_gap_cnt(){
  int ans = 0;
  for (int i = 0; i < T; i++)
    ans += get_teacher_gap_cnt(i);
  return ans;
}

int get_single_bell_cnt(){
  int ans = 0;
  for (int i = 0; i < T; i++){
    int sum = get_teacher_single_bell_cnt(i);
    ans += sum;
  }
  return ans;
}

int get_max_day_cnt(){
  int ans = 0;
  for (int i = 0; i < T; i++)
    ans += get_teacher_max_day_cnt(i);
  return ans;
}

int get_square_unit_sum(){
  int ans = 0;
  int cd_[C][D];
  for (int i = 0; i < C; i++)
    for (int j = 0; j < D; j++)
      cd_[i][j] = 0;
  
  for (int i = 0; i < T; i++)
    for (int j = 0; j < H; j++)
      if (table[i][j] != -1)
	cd_[table[i][j]][j / HOURS_EACH_DAY] += curriculum[which_base[table[i][j]]][course_table[i][j]];

  for (int i = 0; i < C; i++)
    for (int j = 0; j < D; j++)
      ans += pow (cd_[i][j], 2);
  return ans;
}

int get_out_of_free_time_cnt(){
  int ans = 0;
  for (int i = 0; i < T; i++){
    int sum = 0;
    for (int j = 0; j < H; j++)
      if (table[i][j] != -1 and not is_free[i][j])
	sum++;
    ans += sum;
  }
  return ans;
}

int get_super_impose_cnt(){
  int hc_[H][C];
  int ans = 0;

  for (int i = 0; i < H; i++)
    for (int j = 0; j < C; j++)
      hc_[i][j] = 0;
  
  for (int i = 0; i < T; i++)
    for (int j = 0; j < H; j++)
      if (table[i][j] != -1)
	hc_[j][table[i][j]]++;
  for (int i = 0; i < H; i++)
    for (int j = 0; j < C; j++)
      ans += max(0, hc_[i][j] - 1);
  return ans;
}


tuple<double, int, int, int, int, int, int> obj(){
  int gap_cnt = get_gap_cnt();
  int single_bell_cnt = get_single_bell_cnt();
  int max_day_cnt = get_max_day_cnt();
  int square_unit_sum = get_square_unit_sum();
  int out_of_free_time_cnt = get_out_of_free_time_cnt();
  int super_impose_cnt = get_super_impose_cnt();
  double objective = gap_cnt * GW + single_bell_cnt * SBW + max_day_cnt * DW + (square_unit_sum - su_sum_lower_bound) * SUW + W * (out_of_free_time_cnt * OW_I + super_impose_cnt * SW_I);
  return make_tuple(objective, gap_cnt, single_bell_cnt, max_day_cnt, square_unit_sum, out_of_free_time_cnt, super_impose_cnt);
}


void evaluate_schedule(){
  tuple<double, int, int, int, int, int, int> delta = obj();
  cur_obj = get<0>(delta), g_cnt = get<1>(delta), sb_cnt = get<2>(delta);
  md_cnt = get<3>(delta), su_sum = get<4>(delta), o_cnt = get<5>(delta);
  s_cnt = get<6>(delta);
  //reseting the td, cd and hc
  if (td.size() == 0){
    for (int i = 0; i < T; i++){
      Vi new_vec;
      for (int j = 0; j < D; j++)
	new_vec.pb(0);
      td.pb(new_vec);
    }
  }
  else{
    for (int i = 0; i < T; i++)
      for (int j = 0; j < D; j++)
	td[i][j] = 0;
  }
  if (cd.size() == 0){
    for (int i = 0; i < C; i++){
      Vi new_vec;
      for (int j = 0; j < D; j++)
	new_vec.pb(0);
      cd.pb(new_vec);
    }
  }else{
    for (int i = 0; i < C; i++)
      for (int j = 0; j < D; j++)
	cd[i][j] = 0;
  }
  if (hc.size() == 0){
    for (int i = 0; i < H; i++){
      Vi new_vec;
      for (int j = 0; j < C; j++)
	new_vec.pb(0);
      hc.pb(new_vec);
    }
  }else{
    for (int i = 0; i < H; i++)
      for (int j = 0; j < C; j++)
	hc[i][j] = 0;
  }
  
  //filling the td, cd and hc
  for (int i = 0; i < T; i++)
    for (int j = 0; j < H; j++)
      if (table[i][j] != -1)
	td[i][j / HOURS_EACH_DAY]++, cd[table[i][j]][j / HOURS_EACH_DAY] += curriculum[which_base[table[i][j]]][course_table[i][j]], hc[j][table[i][j]]++;
}

void initiate(){
  for (int i = 0; i < T; i++){
    Vi table_vec, course_table_vec;
    // first initializing all time slots to blank i.e. -1
    for (int j = 0; j < H; j++)
      table_vec.pb(-1), course_table_vec.pb(-1);
    table.pb(table_vec), course_table.pb(course_table_vec);
    int a[H];
    for (int j = 0; j < H; j++)
      a[j] = j;
    random_shuffle(a, a + H);
    int cnt = 0;
    for (int j = 0; j < v[i].size(); j++){
      int _course = v[i][j].fi;
      int _class = v[i][j].se;
      int _hours = curriculum[which_base[_class]][_course];
      for (int k = 0; k < _hours; k++)
	table[i][a[cnt]] = _class, course_table[i][a[cnt++]] = _course;
      
    }
  }
  evaluate_schedule();
  BEST = table, BEST_COURSE_TABLE = course_table, BEST_OBJ = cur_obj;


  // assertation of hours of classes of the schedule
  int class_cnt[C];
  for (int i = 0; i < C; i++)
    class_cnt[i] = 0;
  for (int i = 0; i < T; i++)
    for (int j = 0; j < H; j++)
      if (table[i][j] != -1)
	class_cnt[table[i][j]]++; 
  for (int i = 0; i < C; i++)
    assert(class_cnt[i] == H);
  
}


// returns the last_before busy timeslot and fist after, timeslot mat for teacher t in the same day
Pi get_last_before_first_after(int t, int mat){
  int last_before = -1;
  int first_after = -1;
  int day = mat / HOURS_EACH_DAY;
  for (int i = 0; i < HOURS_EACH_DAY; i++){
    int timeslot = day * HOURS_EACH_DAY + i;
    int c = table[t][timeslot];
    if (c != -1 and timeslot < mat)
      last_before = timeslot;
    if (c != -1 and timeslot > mat and first_after == -1)
      first_after = timeslot;
  }
  return {last_before, first_after};
}



int get_rel_gap_cnt(int t, int a, int b){
  if (is_assistant[t])
    return 0;
  int gap_cnt = 0;
  if (table[t][a] != -1 and table[t][b] != -1)
    gap_cnt = 0;
  else{
    int a_day = a / HOURS_EACH_DAY;
    int b_day = b / HOURS_EACH_DAY;
    if (a_day == b_day){
      Vi before;
      for (int i = 0 ; i < HOURS_EACH_DAY; i++)
	if (table[t][a_day * HOURS_EACH_DAY + i] != -1)
	  before.pb(i);
      Vi after;
      if (table[t][a] == -1){
	for (auto item: before)
	  if (item != b % HOURS_EACH_DAY)
	    after.pb(item);
	after.pb(a % HOURS_EACH_DAY);
      }
      else{
	for (auto item: before)
	  if (item != a % HOURS_EACH_DAY)
	    after.pb(item);
	after.pb(b % HOURS_EACH_DAY);
      }
      sort(after.begin(), after.end());
      int after_gap = 0; // gap after move
      int before_gap = 0; // gap before move
      int pre = -1;
      for (auto item: before){
	if(pre != -1)
	  before_gap += item - pre - 1;
	pre = item;
      }

      pre = -1;
      for (auto item: after){
	if (pre != -1)
	  after_gap += item - pre - 1;
	pre = item;
      }
      gap_cnt = after_gap - before_gap;
    }
    else{
      // considering b->a
      Pi p = get_last_before_first_after(t, b);
      int last_before = p.fi, first_after = p.se;
      if (last_before != -1 and first_after != -1)
	gap_cnt += 1;
      else if (last_before != -1)
	gap_cnt -= b - last_before - 1;
      else if (first_after != -1)
	gap_cnt -= first_after - b - 1;

      p = get_last_before_first_after(t, a);
      last_before = p.fi, first_after = p.se;
      if (last_before != -1 and first_after != -1)
	gap_cnt -= 1;
      else if (last_before != -1)
	gap_cnt += a - last_before - 1;
      else if (first_after != -1)
	gap_cnt += first_after - a - 1;
      // if the case a->b then negate the cnt
      if (table[t][b] == -1)
	gap_cnt *= -1;
    }
  }
  return gap_cnt;
}


int get_rel_single_bell_cnt(int t, int a, int b){
  //cout << "in single bell rel func" << endl;
  int single_bell_cnt = 0;
  int ac = get_comp(a); // complement hour of a
  int bc = get_comp(b); // complement hour of b
  if (ac != b){
    if (ac != -1){
      int before = (table[t][ac] != table[t][a]);
      int after = (table[t][ac] != table[t][b]);
      single_bell_cnt += after - before;
    }
    if (bc != -1){
      int before = (table[t][bc] != table[t][b]);
      int after = (table[t][bc] != table[t][a]);
      single_bell_cnt += after - before;
    }
  }
  //cout << "signle bell cnt for move t:" << t << " a: " << a << " b: " << b << " " << single_bell_cnt << endl;
  return single_bell_cnt;
}


int get_rel_day_cnt(int t, int a, int b){
  int days_cnt = 0; // relative day diff
  int prev_day_cnt = 0;
  for (int i = 0; i < D; i++)
    if (td[t][i])
      prev_day_cnt++;
  
  if (a / HOURS_EACH_DAY == b / HOURS_EACH_DAY)
    days_cnt = 0;
  else if (table[t][a] == -1) // move b->a
    days_cnt += max(0, prev_day_cnt + (td[t][a/HOURS_EACH_DAY] == 0) - (td[t][b/HOURS_EACH_DAY] == 1) - teacher_max_days[t]) - max(0, prev_day_cnt - teacher_max_days[t]);
  else if (table[t][b] == -1) // move a->b
    days_cnt += max(0, prev_day_cnt + (td[t][b/HOURS_EACH_DAY] == 0) - (td[t][a/HOURS_EACH_DAY] == 1) - teacher_max_days[t]) - max(0, prev_day_cnt - teacher_max_days[t]);
  return days_cnt;
}


int get_rel_square_unit_sum(int t, int a, int b){
  int cnt = 0;
  if (a / HOURS_EACH_DAY != b / HOURS_EACH_DAY){
    if (table[t][a] != -1){
      int _class = table[t][a];
      int _course = course_table[t][a];
      int _units = curriculum[which_base[_class]][_course];
      cnt += pow(cd[_class][a / HOURS_EACH_DAY] - _units, 2) - pow (cd[_class][a / HOURS_EACH_DAY], 2);
      cnt += pow(cd[_class][b / HOURS_EACH_DAY] + _units, 2) - pow (cd[_class][b / HOURS_EACH_DAY], 2);
    }

    if (table[t][b] != -1){
      int _class = table[t][b];
      int _course = course_table[t][b];
      int _units = curriculum[which_base[_class]][_course];
      cnt += pow(cd[_class][b / HOURS_EACH_DAY] - _units, 2) - pow(cd[_class][b / HOURS_EACH_DAY], 2);
      cnt += pow(cd[_class][a / HOURS_EACH_DAY] + _units, 2) - pow(cd[_class][a / HOURS_EACH_DAY], 2);
    }
  }
  return cnt;
}


int get_rel_out_of_free_time_cnt(int t, int a, int b){
  int out_of_free_time_cnt = 0;
  // move b->a
  if (table[t][a] == -1){
    int before = !is_free[t][b];
    int after = !is_free[t][a];
    out_of_free_time_cnt = after - before;
  }else if (table[t][b] == -1){ // move a->b
    int before = !is_free[t][a];
    int after = !is_free[t][b];
    out_of_free_time_cnt = after - before;
  }
  return out_of_free_time_cnt;
}



int get_rel_super_impose_cnt(int t, int a, int b){
  int super_impose_cnt = 0;
  // a section
  if (table[t][a] != -1)
    super_impose_cnt -= (hc[a][table[t][a]] > 1);
  if (table[t][b] != -1)
    super_impose_cnt += (hc[a][table[t][b]] > 0);
  //b section
  if (table[t][b] != -1)
    super_impose_cnt -= (hc[b][table[t][b]] > 1);
  if (table[t][a] != -1)
    super_impose_cnt += (hc[b][table[t][a]] > 0);
  return super_impose_cnt;
}


// rel_obj for move ,swapping teacher a and b timeslots.
tuple<double, int, int, int, int, int, int>  rel_obj(int t, int a, int b, bool use_dynamic_w){

  int gap_cnt = get_rel_gap_cnt(t, a, b);

  int single_bell_cnt = get_rel_single_bell_cnt(t, a, b);
  
  int days_cnt = get_rel_day_cnt(t, a, b);

  int su_sum_cnt = get_rel_square_unit_sum(t, a, b);

  int out_of_free_time_cnt = get_rel_out_of_free_time_cnt(t, a, b);
  
  int super_impose_cnt = get_rel_super_impose_cnt(t, a, b);

  double rel_obj = gap_cnt * GW + single_bell_cnt * SBW + days_cnt * DW + su_sum_cnt * SUW + W * (super_impose_cnt * (use_dynamic_w ? SW: SW_I) + out_of_free_time_cnt * (use_dynamic_w ? OW: OW_I));

  return make_tuple(rel_obj, gap_cnt, single_bell_cnt, days_cnt,  su_sum_cnt, out_of_free_time_cnt, super_impose_cnt);
}


int get_rel_double_super_impose_cnt(int t1, int a1, int b1, int t2, int a2, int b2){
  // first get the objective value for first move; i.e. t1, a1, b1
  int super_impose_cnt = get_rel_super_impose_cnt(t1, a1, b1);
  unordered_map<int, unordered_map<int, int>> new_hc;
  if (table[t1][a1] != -1)
    new_hc[a1][table[t1][a1]] = hc[a1][table[t1][a1]] - 1, new_hc[b1][table[t1][a1]] = hc[b1][table[t1][a1]] + 1;
  if (table[t1][b1] != -1)
    new_hc[b1][table[t1][b1]] = hc[b1][table[t1][b1]] - 1, new_hc[a1][table[t1][b1]] = hc[a1][table[t1][b1]] + 1;
  				       
  // a section
  
  if (table[t2][a2] != -1){
    if (new_hc.find(a2) != new_hc.end() and new_hc[a2].find(table[t2][a2]) != new_hc[a2].end())
      super_impose_cnt -= (new_hc[a2][table[t2][a2]] > 1);
    else
      super_impose_cnt -= (hc[a2][table[t2][a2]] > 1);
  }
  if (table[t2][b2] != -1){
    if (new_hc.find(a2) != new_hc.end() and new_hc[a2].find(table[t2][b2]) != new_hc[a2].end())
      super_impose_cnt += (new_hc[a2][table[t2][b2]] != 0);
    else
      super_impose_cnt += (hc[a2][table[t2][b2]] != 0);
  }
    
  //b section
  if (table[t2][b2] != -1){
    if (new_hc.find(b2) != new_hc.end() and new_hc[b2].find(table[t2][b2]) != new_hc[b2].end())
      super_impose_cnt -= (new_hc[b2][table[t2][b2]] > 1);
    else
      super_impose_cnt -= (hc[b2][table[t2][b2]] > 1);
  }
    
  if (table[t2][a2] != -1){
    if (new_hc.find(b2) != new_hc.end() and new_hc[b2].find(table[t2][a2]) != new_hc[b2].end())
      super_impose_cnt += (new_hc[b2][table[t2][a2]] != 0);
    else
      super_impose_cnt += (hc[b2][table[t2][a2]] != 0);
  }
  //cout << "double rel s_cnt for move: " << t1 << " " << a1 << " " << b1 << " " << t2 << " " << a2 << " " << b2 << " is: " << super_impose_cnt << endl;
  return super_impose_cnt;
}



int get_rel_double_square_unit_sum(int t1, int a1, int b1, int t2, int a2, int b2){
  int ans = get_rel_square_unit_sum(t1, a1, b1); // get value of first part of the move 
  unordered_map<int, unordered_map<int, int>>  new_cd; // take changes of cd into account and store the changes in the new_cd
  if (a1 / HOURS_EACH_DAY != b1 / HOURS_EACH_DAY){
    if (table[t1][a1] != -1){
      // a -> b
      int _units = curriculum[which_base[table[t1][a1]]][course_table[t1][a1]];
      new_cd[table[t1][a1]][a1 / HOURS_EACH_DAY] = cd[table[t1][a1]][a1 / HOURS_EACH_DAY] - _units;
      new_cd[table[t1][a1]][b1 / HOURS_EACH_DAY] = cd[table[t1][a1]][b1 / HOURS_EACH_DAY] + _units;
    }
    if (table[t1][b1] != -1){
      // b -> a
      int _units = curriculum[which_base[table[t1][b1]]][course_table[t1][b1]];
      new_cd[table[t1][b1]][b1 / HOURS_EACH_DAY] = cd[table[t1][b1]][b1 / HOURS_EACH_DAY] - _units;
      new_cd[table[t1][b1]][a1 / HOURS_EACH_DAY] = cd[table[t1][b1]][a1 / HOURS_EACH_DAY] + _units;
    }
  }

  if (a2 / HOURS_EACH_DAY != b2 / HOURS_EACH_DAY){
    if (table[t2][a2] != -1){
      int _class = table[t2][a2];
      int _course = course_table[t2][a2];
      int _units = curriculum[which_base[_class]][_course];
      if (new_cd.find(_class) != new_cd.end() and new_cd[_class].find(a2 / HOURS_EACH_DAY) != new_cd[_class].end())
	ans += pow(new_cd[_class][a2 / HOURS_EACH_DAY] - _units, 2) - pow (new_cd[_class][a2 / HOURS_EACH_DAY], 2);
      else
	ans += pow(cd[_class][a2 / HOURS_EACH_DAY] - _units, 2) - pow (cd[_class][a2 / HOURS_EACH_DAY], 2);
      if (new_cd.find(_class) != new_cd.end() and new_cd[_class].find(b2 / HOURS_EACH_DAY) != new_cd[_class].end())
	ans += pow(new_cd[_class][b2 / HOURS_EACH_DAY] + _units, 2) - pow (new_cd[_class][b2 / HOURS_EACH_DAY], 2);
      else
	ans += pow(cd[_class][b2 / HOURS_EACH_DAY] + _units, 2) - pow (cd[_class][b2 / HOURS_EACH_DAY], 2);
    }

    if (table[t2][b2] != -1){
      int _class = table[t2][b2];
      int _course = course_table[t2][b2];
      int _units = curriculum[which_base[_class]][_course];
      if (new_cd.find(_class) != new_cd.end() and new_cd[_class].find(b2 / HOURS_EACH_DAY) != new_cd[_class].end())
	ans += pow(new_cd[_class][b2 / HOURS_EACH_DAY] - _units, 2) - pow(new_cd[_class][b2 / HOURS_EACH_DAY], 2);
      else
	ans += pow(cd[_class][b2 / HOURS_EACH_DAY] - _units, 2) - pow(cd[_class][b2 / HOURS_EACH_DAY], 2);
      if (new_cd.find(_class) != new_cd.end() and new_cd[_class].find(a2 / HOURS_EACH_DAY) != new_cd[_class].end())
	ans += pow(new_cd[_class][a2 / HOURS_EACH_DAY] + _units, 2) - pow(new_cd[_class][a2 / HOURS_EACH_DAY], 2);
      else
	ans += pow(cd[_class][a2 / HOURS_EACH_DAY] + _units, 2) - pow(cd[_class][a2 / HOURS_EACH_DAY], 2);
    }
  }
  return ans;
}



tuple<double, int, int, int, int, int, int> double_rel_obj(tuple<int, int, int, int, int, int> move){
  int t1 = get<0>(move), a1 = get<1>(move), b1 = get<2>(move), t2 = get<3>(move), a2 = get<4>(move), b2 = get<5>(move);

  int gap_cnt = get_rel_gap_cnt(t1, a1, b1) + get_rel_gap_cnt(t2, a2, b2);

  int single_bell_cnt = get_rel_single_bell_cnt(t1, a1, b1) + get_rel_single_bell_cnt(t2, a2, b2);
  
  int days_cnt = get_rel_day_cnt(t1, a1, b1) + get_rel_day_cnt(t2, a2, b2);  

  int su_sum_cnt = get_rel_double_square_unit_sum(t1, a1, b1, t2, a2, b2);
  
  int super_impose_cnt = get_rel_double_super_impose_cnt(t1, a1, b1, t2, a2, b2);

  int out_of_free_time_cnt = get_rel_out_of_free_time_cnt(t1, a1, b1) + get_rel_out_of_free_time_cnt(t2, a2, b2);

  double rel_obj = gap_cnt * GW + single_bell_cnt * SBW + days_cnt * DW + su_sum_cnt * SUW + W * (super_impose_cnt * SW_I + out_of_free_time_cnt * OW_I);

  return {rel_obj, gap_cnt, single_bell_cnt, days_cnt, su_sum_cnt, out_of_free_time_cnt, super_impose_cnt};
}




tuple<int, int, int, int, int, int> get_random_double_move(){
  int t1 = -1, a1 = -1, b1 = -1, t2 = -1, a2 = -1, b2 = -1;
  do{
    t1 = rand(0, T - 1);
    a1 = rand(0, H - 1);
    do{
      b1 = rand(0 , H - 1);
    }
    while(b1 == a1);
    
  }while(table[t1][a1] == table[t1][b1]); // generating a proper single move

  if (table[t1][a1] != -1 and hc[b1][table[t1][a1]]){
    // fix the table in b1
    a2 = b1;
    for (int i = 0; i < T; i++)
      if (table[i][a2] == table[t1][a1]){
	t2 = i;
	break;
      }
    do{
      b2 = rand(0, H - 1);
    }while(b2 == a2);
    if (table[t2][a2] == table[t2][b2])
      t2 = a2 = b2 = -1;
  }else if (table[t1][b1] != -1 and hc[a1][table[t1][b1]]){
    // fix the table in a1
    a2 = a1;
    for (int i = 0; i < T; i++)
      if (table[i][a2] == table[t1][b1]){
	t2 = i;
	break;
      }
    do{
      b2 = rand(0, H - 1);
    }while(b2 == a2);
    if (table[t2][a2] == table[t2][b2])
      t2 = a2 = b2 = -1;
  }
    
  
  return {t1, a1, b1, t2, a2, b2};
}

void update_tables_and_td_hc_cd(int t, int a, int b){
  if (a / HOURS_EACH_DAY != b / HOURS_EACH_DAY){
    if (table[t][a] == -1)
      td[t][a / HOURS_EACH_DAY]++, td[t][b / HOURS_EACH_DAY]--;
    else if (table[t][b] == -1)
      td[t][b / HOURS_EACH_DAY]++, td[t][a / HOURS_EACH_DAY]--;
  }

  if (table[t][a] != -1)
    hc[a][table[t][a]]--, hc[b][table[t][a]]++;
  if (table[t][b] != -1)
    hc[b][table[t][b]]--, hc[a][table[t][b]]++;


  int a_units = 0, b_units = 0;
  if (table[t][a] != -1)
    a_units = curriculum[which_base[table[t][a]]][course_table[t][a]];
  if (table[t][b] != -1)
    b_units = curriculum[which_base[table[t][b]]][course_table[t][b]];
  
  if (a / HOURS_EACH_DAY != b / HOURS_EACH_DAY){
    if (table[t][a] != -1)
      cd[table[t][a]][a / HOURS_EACH_DAY] -= a_units, cd[table[t][a]][b / HOURS_EACH_DAY] += a_units;
    if (table[t][b] != -1)
      cd[table[t][b]][b / HOURS_EACH_DAY] -= b_units, cd[table[t][b]][a / HOURS_EACH_DAY] += b_units;
  }
  int temp = table[t][a];
  table[t][a] = table[t][b];
  table[t][b] = temp;

  temp = course_table[t][a];
  course_table[t][a] = course_table[t][b];
  course_table[t][b] = temp;
  
}
void perform_double_move(tuple<int, int, int, int, int, int> move, tuple<double, int, int, int, int, int, int> crude_delta){
  update_tables_and_td_hc_cd(get<0>(move), get<1>(move), get<2>(move));
  update_tables_and_td_hc_cd(get<3>(move), get<4>(move), get<5>(move));
  
  cur_obj += get<0>(crude_delta);
  g_cnt += get<1>(crude_delta);
  sb_cnt += get<2>(crude_delta);
  md_cnt += get<3>(crude_delta);
  su_sum += get<4>(crude_delta);
  o_cnt += get<5>(crude_delta);
  s_cnt += get<6>(crude_delta);
}

void perform_move(int t, int a, int b, tuple <double, int, int, int, int, int, int> crude_delta){
  update_tables_and_td_hc_cd(t, a, b);
  
  cur_obj += get<0>(crude_delta);
  g_cnt += get<1>(crude_delta);
  sb_cnt += get<2>(crude_delta);
  md_cnt += get<3>(crude_delta);
  su_sum += get<4>(crude_delta);
  o_cnt += get<5>(crude_delta);
  s_cnt += get<6>(crude_delta);
}

void update_dynamic_weights(){
  double o_factor = rand_double(O_ALPHA_MIN, O_ALPHA_MAX);
  double s_factor = rand_double(S_ALPHA_MIN, S_ALPHA_MAX);
  cout << "o factor " << o_factor << " s factor " << s_factor << endl;
  if (LOG_O_SET)
    OW *= o_factor;
  else if (LOG_O_RESET)
    OW /= o_factor;
  if(LOG_S_SET)
    SW *= s_factor;
  else if (LOG_S_RESET)
    SW /= s_factor;
  OW = min(OW, OW_MAX);
  OW = max(OW, OW_MIN);
  SW = min(SW, SW_MAX);
  SW = max(SW, SW_MIN);
  LOG_O_SET = LOG_O_RESET = LOG_S_SET = LOG_S_RESET = 1;
}

void revert_to_best(){
  table = BEST, course_table = BEST_COURSE_TABLE;
  evaluate_schedule();
}

double avg = 0;
double rna_avg = 0;
int rna_iteration = 0;


tuple<double, int, int, int, int, int, int> get_crude_obj(tuple<double, int, int, int, int, int, int> obj){
  double crude_obj = get<1>(obj) * GW + get<2>(obj) * SBW + get<3>(obj) * DW + get<4>(obj) * SUW + W * (get<6>(obj) * SW_I + get<5>(obj) * OW_I);
  return make_tuple(crude_obj, get<1>(obj), get<2>(obj), get<3>(obj), get<4>(obj), get<5>(obj), get<6>(obj));
}


void func(promise<tuple<double, int, int, int>> &&prms, int start, int end){
  // start: start index of teacher that is going to be analysed in this thread[inclusive]
  // end: end index of teacher that is going to be analysed in this thread[exclusive]
  tuple <int, int, int> best_move = {-1, -1, -1};
  double best_obj = INF;
  for (int i = start; i < end; i++)
    for (int j = 0; j < H; j++)
      for (int k = j + 1; k < H; k++){
	if (table[i][j] == table[i][k])
	  continue;
	tuple<double, int, int, int, int, int, int> objective = rel_obj(i, j, k, true);
	tuple<double, int, int, int, int, int, int> crude_objective = get_crude_obj(objective);
	if ((get<0>(crude_objective) + cur_obj < BEST_OBJ or tabu[i][j][k] < now) and (get<0>(objective) < best_obj))
	  best_move = {i, j, k}, best_obj = get<0>(objective);
      }
  prms.set_value(make_tuple(best_obj, get<0>(best_move), get<1>(best_move), get<2>(best_move)));
}


void extract_potential_feasible_schedule(){
  if (o_cnt == 0 and s_cnt == 0 and cur_obj < BEST_F_OBJ){
    print_schedule("schedule" + to_string((int)feasible_schedules.size()));
    feasible_schedules.pb(make_pair(table, course_table));
    BEST_F_OBJ = cur_obj;
  }
  
}

void search(){
  //cout << g_cnt << " " << sb_cnt << " " << md_cnt << " " << su_sum << " " << o_cnt << " " << s_cnt << endl;
      
  idle_cycles = 0;
  while (idle_cycles < MAX_CYCLES){
    idle_cycles++;
    idle = 0;
    // RNA phase

    rna_iteration = 0;
    while (idle < RNA_MAX){
      //auto start = high_resolution_clock::now();
      idle++;
      tuple<int, int, int, int, int, int> move = get_random_double_move();
      tuple<double, int, int, int, int, int, int>  delta;
      if (get<3>(move) != -1)
	delta = double_rel_obj(move);
      else
	delta = rel_obj(get<0>(move), get<1>(move), get<2>(move), false);
      
      if (get<0>(delta) <= 0){
	if(get<3>(move) != -1)
	  perform_double_move(move, delta);
	else
	  perform_move(get<0>(move), get<1>(move), get<2>(move), delta);
      }
      extract_potential_feasible_schedule();
      //tuple<double, int, int, int, int, int, int> true_values = obj();
      cout << "RNA: " << g_cnt << " " << sb_cnt << " " << md_cnt << " " << su_sum << " " << o_cnt << " " << s_cnt << " objective " << cur_obj << " idle " << idle << endl;
      //assert(g_cnt == get<1>(true_values) and sb_cnt == get<2>(true_values) and md_cnt == get<3>(true_values) and su_sum == get<4>(true_values) and o_cnt == get<5>(true_values) and s_cnt == get<6>(true_values));
      //cout << "true objective " << get<0>(true_values) << endl;
      
      if (get<0>(delta) < 0)
	idle = 0, idle_cycles = 0;
      //auto stop = high_resolution_clock::now();
      //rna_avg += duration_cast<microseconds>(stop - start).count();
      //rna_iteration++;
      //if (rna_iteration == 100){
      //rna_avg /= 100.;
      //cout << "rna average excution time is: " << rna_avg << endl;
	//exit(0);
      //}
    }

    
    
    BEST = table, BEST_COURSE_TABLE = course_table, BEST_OBJ = cur_obj;
    
    // TS phase
    idle = 0;
    // reset the params to initial value
    for (int i = 0; i < T; i++)
      for (int j = 0; j < H; j++)
	for (int k = j + 1; k < H; k++)
	  tabu[i][j][k] = now - 1;
    LOG_O_SET = LOG_O_RESET = LOG_S_SET = LOG_S_RESET = 1;
    SW = SW_I, OW = OW_I;
    
    while (idle < TS_MAX){
      //auto start = high_resolution_clock::now();
      idle++;
      tuple<int, int, int> best_move = {-1, -1, -1};
      double best_obj = INF; // rel_obj
      promise<tuple<double, int, int, int>> promises[proc_cnt];
      future<tuple<double, int, int, int>> futures[proc_cnt];
      thread threads[proc_cnt];
      for (int i = 0; i < proc_cnt; i++)
	promises[i] = promise<tuple<double, int, int, int>>(), futures[i] = promises[i].get_future(), threads[i] = thread(&func, move(promises[i]), T * i / proc_cnt, T * (i + 1) / proc_cnt);

      tuple<double, int, int, int> ans;
      for (int i = 0; i < proc_cnt; i++)
	ans = futures[i].get(), get<0>(ans) < best_obj ? best_move = {get<1>(ans), get<2>(ans), get<3>(ans)}, best_obj = get<0>(ans):1;
      
      for (int i = 0; i < proc_cnt; i++)
	threads[i].join();
      
      if (get<0>(best_move) == -1){
	cout << "no move to perform!!!! all moves tabu:) " << endl;
	exit(0);
      }

      tuple<double, int, int, int, int, int, int>  delta = rel_obj(get<0>(best_move), get<1>(best_move), get<2>(best_move), false);
      perform_move(get<0>(best_move), get<1>(best_move), get<2>(best_move), delta), tabu[get<0>(best_move)][get<1>(best_move)][get<2>(best_move)] = now + rand(TENURE_MIN, TENURE_MAX);
      if (cur_obj < BEST_OBJ)
	BEST = table, BEST_COURSE_TABLE = course_table, BEST_OBJ = cur_obj, idle = 0, idle_cycles = 0;
      now++;
      cout << "TS: " << g_cnt << " " << sb_cnt << " " << md_cnt << " " << su_sum << " " << o_cnt << " " << s_cnt << " objective: " << cur_obj << " idle: " << idle << endl;
      cout << "dynamic weights: OW " << OW << " SW " << SW << endl;
      cout << "______________" << endl;
      //tuple<double, int, int, int, int, int, int> true_values = obj();
      //assert(g_cnt == get<1>(true_values) and sb_cnt == get<2>(true_values) and md_cnt == get<3>(true_values) and su_sum == get<4>(true_values) and o_cnt == get<5>(true_values) and s_cnt == get<6>(true_values));
      if (s_cnt)
	LOG_S_RESET = 0;
      else
	LOG_S_SET = 0;
      if (o_cnt)
	LOG_O_RESET = 0;
      else
	LOG_O_SET = 0;
      if(!(now % K))
	update_dynamic_weights();
      extract_potential_feasible_schedule();
      //auto stop = high_resolution_clock::now();
      //avg += duration_cast<microseconds>(stop - start).count();
      //if (now == 100){
      //avg /= 100.;
      //cout << "average excution time of tabu iteration is: " << avg << endl;
      //exit(0);
      //}
    }
    revert_to_best();
  }
  signal_callback_handler(10);
}



void fill_inputs(){
  fstream fin;
  fin.open("./config_file", ios::in); 
  fin >> D >> HOURS_EACH_DAY >> R >> B >> T >> C;
  H = D * HOURS_EACH_DAY;
  for (int i = 0; i < D; i++)
    for (int j = 0; j < HOURS_EACH_DAY; j++)
      time_[i * HOURS_EACH_DAY + j] = day[i] + "/" + to_string(j + 1);
  
  for (int i = 0; i < D; i++)
    h_.pb(HOURS_EACH_DAY);

  for (int i = 0; i < R; i++){
    fin >> course[i];
    r_string_to_num[course[i]] = i;
    course_name_set.insert(course[i]);
  }
  
  int class_cnt_sum = 0; // sum of class cnt
  for (int i = 0; i < B; i++){
    fin >> base[i];
    fin >> b[i]; // get number of classes of the base i
    class_cnt_sum += b[i];
    int units;
    string course_name;
    int size; // curriculum size for this base
    fin >> size;
    int hour_sum = 0; // this base's curriculum hour sum
    for(int j = 0; j < size; j++){
      fin >> course_name >> units;
      hour_sum += units;
      try{
	curriculum[i][r_string_to_num[course_name]] = units;
      }catch(...){
	cout << "No course name: " << course_name << endl;
	exit(0);
      }
    }
    if (hour_sum != D * HOURS_EACH_DAY){
      cout << "hour_sum missmatch for curriculum of " << base[i] << ". should be:  " << D * HOURS_EACH_DAY << " but is: " << hour_sum << endl;
      exit(0);
    }
  }
  if (class_cnt_sum != C){
    cout << "number of classes missmatch with the overall class cnt" << endl;
    exit(0);
  }
  
  int cnt = 0;
  for (int i = 0; i < B; i++)
    for (int j = 0; j < b[i]; j++)
      which_base[cnt] = i, class_[cnt++] = base[i] + to_string(j);
  
  int free_time_days_cnt;
  for (int i = 0; i < T; i++){
    int fields_cnt;
    fin >> teacher[i] >> h[i] >> teacher_max_days[i] >> free_time_days_cnt >> fields_cnt;
    t_string_to_num[teacher[i]] = i;
    if (is_substring("assistant", teacher[i]))
      is_assistant[i] = true;
    
    int free_hours_cnt;
    int free_hours_sum = 0;
    for (int j = 0; j < free_time_days_cnt; j++){
      int day;
      fin >> day >> free_hours_cnt;
      free_hours_sum += free_hours_cnt;
      int hour;
      for (int k = 0; k < free_hours_cnt; k++){
	fin >> hour;
	is_free[i][day * HOURS_EACH_DAY + hour] = 1;
      }
    }
    if (free_hours_sum < h[i]){
      cout << "not enough free time for teacher " << teacher[i] << " : free time " << free_hours_sum << " but obligatory teaching hours " << h[i] << endl;
      exit(0);
    }
    string field_name;
    int hour;
    for (int j = 0; j < fields_cnt; j++){
      fin >> field_name >> hour;
      field[i][field_name] = hour;
    }
    vector<string> covered_stuff;
    int hour_sum = 0;
    for (auto item: field[i]){
      string field_name = item.fi;
      int hour = item.se;
      if (course_name_set.find(field_name) != course_name_set.end())
	continue;
      hour_sum += hour;
      covered_stuff.pb(field_name);
    }
    for (auto item: field[i]){
      string field_name = item.fi;
      int hour = item.se;
      if (course_name_set.find(field_name) == course_name_set.end())
	continue;
      bool covered = false;
      for (auto stuff: covered_stuff)
	if (is_substring(stuff, field_name)){
	  covered = true;
	  break;
	}
      if (not covered)
	hour_sum += hour;
    }
    if (hour_sum != h[i]){
      cout << "hour sum for teacher " << i << " " << teacher[i] << " incorrect. calculated hour_sum: " << hour_sum << " but expected: " << h[i] << endl;
      exit(0);																 
    }
  }

  int teacher_hour_sum = 0; // total sum of teachers teaching hours 
  for (int i = 0; i < T; i++)
    teacher_hour_sum += h[i];
  if (teacher_hour_sum != D * HOURS_EACH_DAY * C){
    cout << "school total teaching hour sum is incorrect with respect to teachers teaching hours. calculated:  " << teacher_hour_sum << "expected: " << C * D * HOURS_EACH_DAY << endl;
    exit(0);
  }
}






vector<tuple<int, int, int>> get_eligible_course_classes(vector<unordered_map<int, int>> remaining_curriculum, Vi course_list){
  vector<tuple<int, int, int>> ans;
  for (int i = 0; i < C; i++){
    for (auto item: remaining_curriculum[i]){
      int course_ = item.fi;
      int hour_ =  item.se;
      bool eligible = false;
      for (int j = 0; j < course_list.size(); j++)
	if (course_list[j] == course_){
	  eligible = true;
	  break;
	}
      if (eligible)
	ans.pb({hour_, course_, i});
    }
  }
  return ans;
}


void assign_teacher_courses(vector<unordered_map<int, int>> &remaining_curriculum, bool consider_only_specific_courses){
  for (int i = 0; i < T; i++){
    for (auto item: hour_spec[i]){
      Vi course_list = item.fi;
      int hours = item.se;
      if (not((course_list.size() > 1) ^ consider_only_specific_courses))
	continue;
      vector<tuple<int, int, int>> ecc = get_eligible_course_classes(remaining_curriculum, course_list);
      string best_hour_pattern_summary = "0";
      int n = ecc.size();
      for (int j = 0; j < pow(2, n); j++){
	int l = j;
	vector<bool> bits;
	int sum = 0;
	for (int k = 0; k < n; k++){
	  bits.pb(l % 2);
	  if (bits[k])
	    sum += get<0>(ecc[k]);
	  l /= 2;
	}
	if (sum == hours){
	  Vi hour_pattern;
	  for (int ll = 0; ll < n; ll++)
	    if (bits[ll])
	      hour_pattern.pb(get<0>(ecc[ll]));
	  sort(hour_pattern.begin(), hour_pattern.end(), greater<>());
	  string summary;
	  for (int ll = 0; ll < hour_pattern.size(); ll++)
	    summary += ('0' + hour_pattern[ll]);
	  if (summary > best_hour_pattern_summary){
	    best_hour_pattern_summary = summary;
	  }
	}
      }

      vector<vector<bool>> best_bits_ones;
      for (int j = 0; j < pow(2, n); j++){
	int l = j;
	vector<bool> bits;
	int sum = 0;
	for (int k = 0; k < n; k++){
	  bits.pb(l % 2);
	  if (bits[k])
	    sum += get<0>(ecc[k]);
	  l /= 2;
	}
	if (sum == hours){
	  Vi hour_pattern;
	  for (int ll = 0; ll < n; ll++)
	    if (bits[ll])
	      hour_pattern.pb(get<0>(ecc[ll]));
	  sort(hour_pattern.begin(), hour_pattern.end(), greater<>());
	  string summary;
	  for (int ll = 0; ll < hour_pattern.size(); ll++)
	    summary += ('0' + hour_pattern[ll]);
	  if (summary == best_hour_pattern_summary){
	    best_bits_ones.pb(bits);
	  }
	}
      }
      vector<bool> best_bits;
      if (best_bits_ones.size() != 0)
	best_bits = best_bits_ones[rand(0, best_bits_ones.size() - 1)];
      
      assert(best_bits.size() != 0);
      for (int ll = 0; ll < best_bits.size(); ll++)
	  if (best_bits[ll])
	    v[i].pb(MP(get<1>(ecc[ll]), get<2>(ecc[ll]))), remaining_curriculum[get<2>(ecc[ll])].erase(get<1>(ecc[ll]));
	// push back the (course, class) pair to the requirement vector and erase the corresponding value from the remaining curriculum
    }
  }
}


void normalize_teacher_fields(){
  for (int i = 0; i < T; i++){
    vector<Pi> sizes;
    for (auto item: field[i]){
      string field_name = item.fi;
      int hour = item.se;
      Vi course_list;
      if (course_name_set.find(field_name) != course_name_set.end()){
	course_list.pb(r_string_to_num[field_name]);
      }else{
	for (auto c: course)
	  if (is_substring(field_name, c))
	    course_list.pb(r_string_to_num[c]);
      }
      sizes.pb(MP(course_list.size(), hour_spec[i].size())); // push_back the (size, index) for each of the course_lists in order to sort them
      hour_spec[i].pb(MP(course_list, hour));
    }
    sort(sizes.begin(), sizes.end());
    unordered_map<int, int> covered_courses; // how many hours do single courses require
    for (int j = 0; j < sizes.size(); j++){
      int index = sizes[j].se;
      Vi course_list = hour_spec[i][index].fi;
      int hour = hour_spec[i][index].se;
      Vi new_course_list;
      for (auto course_: course_list){
	if (covered_courses.find(course_) == covered_courses.end())
	  new_course_list.pb(course_);
	else
	  hour -= covered_courses[course_];
	if (course_list.size() == 1)
	  covered_courses[course_] = hour;
      }
      hour_spec[i][index] = MP(new_course_list, hour);
    }
  }
}



/*TODO:[NOTE] this function should be altered for schools with different hour count on different days.*/
int get_su_sum_lower_bound(){
  double ans = 0;
  for (int i = 0; i < C; i++){
    int sum = 0;
    for (auto item: curriculum[which_base[i]])
      sum += item.se * item.se;
    ans += D * pow(1. * sum / D, 2);
  }
  return (int)floor(ans);
}

signed main(){
  ios_base::sync_with_stdio(false); 
  cin.tie(NULL);
  srand(time(0));
  signal(SIGINT, signal_callback_handler);
  proc_cnt = omp_get_num_procs();
  
  day = {
    {0, "Saturday"}, 
    {1, "Sunday"},
    {2, "Monday"},
    {3, "Tuesday"},
    {4, "Wednesday"},
    {5, "Thursday"},
    {6, "Friday"}
  };
  
  fill_inputs();
  normalize_teacher_fields(); // course_lists should not contain overlaping courses with each other  

  su_sum_lower_bound = get_su_sum_lower_bound();
  cout << "su_sum lower bound is: " << su_sum_lower_bound << endl;
  vector<unordered_map<int, int>> remaining_curriculum;
  for (int i = 0; i < B; i++)
    for (int j = 0; j < b[i]; j++)
      remaining_curriculum.pb(curriculum[i]);
  
  assign_teacher_courses(remaining_curriculum, true);
  assign_teacher_courses(remaining_curriculum, false);


  int class_hour_cnt[C];
  for (int i = 0; i < C; i++)
    class_hour_cnt[i] = 0;


  for (int i = 0; i < T; i++){
    for (int j = 0; j < v[i].size(); j++)
      cout << v[i][j].fi << " " << v[i][j].se << endl;
    cout << "__________" << endl;
  }
  
  for (int i = 0; i < T; i++)
    for (int j = 0; j < v[i].size(); j++){
      int course_ = v[i][j].fi;
      int class_ = v[i][j].se;
      class_hour_cnt[class_] += curriculum[which_base[class_]][course_]; 
    }
  for (int i = 0; i < C; i++)
    cout << class_hour_cnt[i] << " ";
  cout << endl;
  for (int i = 0; i < C; i++)
    assert(class_hour_cnt[i] == H);

  initiate();


  /*
  
  string file_name = "initial_schedule.csv";
  export_to_csv(file_name, h_);
  cout << "objective is: " <<cur_obj << endl;
  pair<vector<Vi>, vector<Vi>> ans = import_from_csv(file_name);
  for (int i = 0; i < ans.fi.size(); i++){
    for (int j = 0; j < ans.fi[i].size(); j++)
      assert((ans.fi[i][j] == table[i][j]) and (ans.se[i][j] == course_table[i][j]));
  }
  table = ans.fi;
  course_table = ans.se;
  evaluate_schedule();
  cout << "obj after retreival is: " << get<0>(obj()) << endl;
  */
  
  search();
  
}
